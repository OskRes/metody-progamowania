#include <iostream>
#include <cstdarg>
#include "Punkt.h"

using namespace std;

int main() {
  punkt p(2, 3);
  cout << p.x() << ' ' << p.y() << '\n';
  p.x() = 1;
  p.y() = 1;
  return 0;
}
