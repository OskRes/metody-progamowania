#include "Punkt.h"
#include <iostream>
#include <ostream>
#include <math.h>

punkt::punkt() : x_(0), y_(0) {};

punkt::punkt(double a, double b) : x_(a), y_(b) {};

punkt::punkt(const punkt *p) {
  this->x_ = p->x_;
  this->y_ = p->y_;
};

double &punkt::x() {
  return x_;
}

const double &punkt::x() const {
  return x_;
}

double &punkt::y() {
  return y_;
}

const double &punkt::y() const {
  return y_;
}

double punkt::odleglosc(const punkt &p) const {
  return sqrt(pow((x() - p.x()), 2) + pow((x() - p.x()), 2));
}

std::ostream &punkt::operator<<(std::ostream &out) {
  return out << "x = " << x() << ", y = " << y() << '\n';
}
