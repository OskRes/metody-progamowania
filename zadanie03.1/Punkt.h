#ifndef PUNKT_H
#define PUNKT_H

class punkt {
  double x_, y_;
public:
  punkt();

  punkt(double a, double b);

  punkt(const punkt *p);

  double &x();

  const double &x() const;

  double &y();

  const double &y() const;

  double odleglosc(const punkt &p) const;

  std::ostream &operator<<(std::ostream &out);
};

#endif
