#include <iostream>
#include "Punkt.h"

class wielobok {
  unsigned iloscWierzcholkow;
  punkt *punkty_;
public:
  wielobok() : iloscWierzcholkow(0), punkty_(0) {}

  wielobok(const punkt *b, const punkt *e) : iloscWierzcholkow(e - b),
                                             punkty_(iloscWierzcholkow ? new punkt[iloscWierzcholkow] : 0) {
    for (unsigned i = 0; i < iloscWierzcholkow; i++) {
      punkty_[i] = new punkt(b + i);
    }
  }

  ~wielobok() {
    delete[] punkty_;
  }

  int ilosc() {
    return iloscWierzcholkow;
  }

  void wierzkolki() {
    std::cout << "początek listy wierzołków" << std::endl;
    for (unsigned i = 0; i < iloscWierzcholkow; i++)
      std::cout << "    " << punkty_[i];
    std::cout << "koniec listy wierzołków" << std::endl;
  }

  const double obwod() const {
    if (iloscWierzcholkow > 2) {
      double tmp = 0.0;
      for (unsigned i = 1; i < iloscWierzcholkow; i++) {
        tmp += (punkty_[i]).odleglosc(punkty_[i - 1]);
        std::cout << tmp << " i: " << i << std::endl;
      }
      tmp += (punkty_[iloscWierzcholkow - 1]).odleglosc(punkty_[0]);
      return tmp;
    }
    return 0.0;
  }

  punkt Punkt(const int i) {
    return punkty_[i - 1];
  }

  void Punkty(const punkt *p1, const punkt *p2) {
    unsigned nowe = p2 - p1;
    if (nowe) {
      punkt *punkty_new = new punkt[iloscWierzcholkow + nowe];
      for (unsigned i = 0; i < iloscWierzcholkow; i++)
        punkty_new[i] = punkt(punkty_[i]);
      delete[] punkty_;
      punkty_ = punkty_new;
      for (unsigned i = iloscWierzcholkow; i < nowe + iloscWierzcholkow; i++)
        punkty_[i] = new punkt(p1 + i);
    }
  }
};



