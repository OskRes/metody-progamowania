#include "Prostokat.h"

class graniastoslup {
  prostokat* podstawa;
  double h_;
public:
  graniastoslup():podstawa(new prostokat()),h_(0){}
  graniastoslup(const double wx, const double wy, const double wz, const double a, const double b, const double h):podstawa(new prostokat(wx, wy, wz, a, b)), h_(h){}
  graniastoslup(const punkt& p, const double a, const double b, const double h):podstawa(new prostokat(p, a, b)), h_(h){}
  graniastoslup(const prostokat& p, const double h):podstawa(new prostokat(p)), h_(h){}
  graniastoslup(const graniastoslup& p);
  ~graniastoslup(){delete podstawa;};
  graniastoslup& operator=(const graniastoslup& p);
  double& x(){
    return (*podstawa).x();
  }
  const double& x() const{
    return (*podstawa).x();
  }
  double& y(){
    return (*podstawa).y();
  }
  const double& y() const{
    return (*podstawa).y();
  }
  double& z(){
    return (*podstawa).z();
  }
  const double& z() const{
    return (*podstawa).z();
  }
  double& a(){
    return (*podstawa).a();
  }
  const double& a() const{
    return (*podstawa).a();
  }
  double& b(){
    return (*podstawa).b();
  }
  const double& b() const{
    return (*podstawa).b();
  }
  double& h(){
    return h_;
  }
  const double& h() const{
    return h_;
  }
  double objetosc(){
    return (*podstawa).pole()*h_;
  }
};
