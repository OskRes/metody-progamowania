#include "Punkt.h"

class prostokat {
  double a_, b_;
  punkt *wierzcholek;
public:
  prostokat() : a_(0), b_(0), wierzcholek(new punkt()) {}

  prostokat(const double wx, const double wy, const double wz, const double a, const double b)
      : a_(0), b_(0), wierzcholek(new punkt(wx, wy, wz)) {}

  prostokat(const punkt p, const double a, const double b) : a_(a), b_(b), wierzcholek(new punkt(p)) {}

  prostokat(const prostokat &src) : a_(src.a_), b_(src.a_), wierzcholek(new punkt(src.x(), src.y(), src.z())) {}

  prostokat &operator=(const prostokat &);

  ~prostokat() {
    delete wierzcholek;
  }

  double &x() {
    return (*wierzcholek).x();
  }

  const double &x() const {
    return (*wierzcholek).x();
  }

  double &y() {
    return (*wierzcholek).y();
  }

  const double &y() const {
    return (*wierzcholek).y();
  }

  double &z() {
    return (*wierzcholek).z();
  }

  const double &z() const {
    return (*wierzcholek).z();
  }

  double &a() {
    return a_;
  }

  const double &a() const {
    return a_;
  }

  double &b() {
    return b_;
  }

  const double &b() const {
    return b_;
  }

  const double pole() const {
    return a_ * b_;
  }
};
