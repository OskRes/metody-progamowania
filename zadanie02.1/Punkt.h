class punkt {
  double x_, y_, z_;
public:
  punkt() : x_(0), y_(0), z_(0) {}

  punkt(const double &a, const double &b, const double &c) : x_(a), y_(b), z_(c) {}

  double &x() {
    return x_;
  }

  const double &x() const {
    return x_;
  }

  double &y() {
    return y_;
  }

  const double &y() const {
    return y_;
  }

  double &z() {
    return z_;
  }

  const double &z() const {
    return z_;
  }
};
