#include <iostream>
#include "osoba.h"

osoba::osoba() : nazwisko_("brak nazwiska"), liczba_lat_(0) {}

osoba::osoba(std::string s, int i) : nazwisko_(s), liczba_lat_(i) {}

const std::string &osoba::nazwisko() const { return nazwisko_; }

std::string &osoba::nazwisko() { return nazwisko_; }

const int osoba::liczba_lat() const { return liczba_lat_; }

int osoba::liczba_lat() { return liczba_lat_; }

void osoba::pokaz() { std::cout << "Nazwisko: " << nazwisko_ << ", liczba lat: " << liczba_lat_ << std::endl; }

std::ostream &operator<<(std::ostream o, const osoba os) {
  return o << "Nazwisko: " << os.nazwisko_ << ", liczba lat: " << os.liczba_lat_ << std::endl;
}

