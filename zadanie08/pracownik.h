#ifndef PRACOWNIK_H
#define PRACOWNIK_H

#include "osoba.h"

class pracownik : public osoba {
  std::string stanowisko_;
  double placa_;
public:
  pracownik();

  pracownik(std::string, int, std::string, double);

  const std::string &stanowisko() const;

  std::string &stanowisko();

  const int placa() const;

  int placa();

  void pokaz();

  friend std::ostream &operator<<(std::ostream, pracownik);
};

#endif
