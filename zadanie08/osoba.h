#ifndef OSOBA_H
#define OSOBA_H

class osoba {
  std::string nazwisko_;
  int liczba_lat_;

public:
  osoba();

  osoba(const std::string, int);

  const std::string &nazwisko() const;

  std::string &nazwisko();

  const int liczba_lat() const;

  int liczba_lat();

  void pokaz();

  friend std::ostream &operator<<(std::ostream, const osoba);
};

#endif
