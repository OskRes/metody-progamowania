#include <iostream>
#include "pracownik.h"

pracownik::pracownik() : osoba(), stanowisko_("brak stanowiska"), placa_(0.0) {}

pracownik::pracownik(std::string n, int w, std::string s, double p) : osoba(n, w), stanowisko_(s), placa_(p) {}

const std::string &pracownik::stanowisko() const { return stanowisko_; }

std::string &pracownik::stanowisko() { return stanowisko_; }

const int pracownik::placa() const { return placa_; }

int pracownik::placa() { return placa_; }

void pracownik::pokaz() {
//    osoba::pokaz();
  std::cout << "stanowisko: " << stanowisko_ << ", płaca: " << placa_ << std::endl;
}

std::ostream &operator<<(std::ostream o, pracownik p) {
  return o << "stanowisko: " << p.stanowisko_ << ", płaca: " << p.placa_ << std::endl;
}
