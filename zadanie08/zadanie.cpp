#include <iostream>

class osoba {
  std::string nazwisko_;
  int liczba_lat_;

public:
  osoba();

  osoba(const std::string, int);

  const std::string &nazwisko() const;

  std::string &nazwisko();

  const int liczba_lat() const;

  int &liczba_lat();

  void pokaz();

  friend std::ostream &operator<<(std::ostream, const osoba);
};

osoba::osoba() : nazwisko_("brak nazwiska"), liczba_lat_(0) {}

osoba::osoba(const std::string s, int i) : nazwisko_(s), liczba_lat_(i) {}

const std::string &osoba::nazwisko() const { return nazwisko_; }

std::string &osoba::nazwisko() { return nazwisko_; }

const int osoba::liczba_lat() const { return liczba_lat_; }

int &osoba::liczba_lat() { return liczba_lat_; }

void osoba::pokaz() { std::cout << "Nazwisko: " << nazwisko_ << ", liczba lat: " << liczba_lat_ << std::endl; }

std::ostream &operator<<(std::ostream o, const osoba os) {
  return o << "Nazwisko: " << os.nazwisko_ << ", liczba lat: " << os.liczba_lat_ << std::endl;
}

class pracownik : public osoba {
  std::string stanowisko_;
  double placa_;
public:
  pracownik();

  pracownik(std::string, int, std::string, double);

  const std::string &stanowisko() const;

  std::string &stanowisko();

  const double placa() const;

  double placa();

  void pokaz();

  friend std::ostream &operator<<(std::ostream &, const pracownik &);

//    friend pracownik &operator=(pracownik &, pracownik &);
};

pracownik::pracownik() : osoba(), stanowisko_("brak stanowiska"), placa_(0.0) {}

pracownik::pracownik(std::string n, int w, std::string s, double p) : osoba(n, w), stanowisko_(s), placa_(p) {}

const std::string &pracownik::stanowisko() const { return stanowisko_; }

std::string &pracownik::stanowisko() { return stanowisko_; }

const double pracownik::placa() const { return placa_; }

double pracownik::placa() { return placa_; }

void pracownik::pokaz() {
  osoba::pokaz();
  std::cout << "stanowisko: " << stanowisko_ << ", płaca: " << placa_ << std::endl;
}

std::ostream &operator<<(std::ostream &o, const pracownik &p) {
//    o << static_cast<const osoba&>(p);
  return o << "stanowisko: " << p.stanowisko_ << ", płaca: " << p.placa_ << std::endl;
}

//pracownik &operator=(pracownik &pracownik1, pracownik &pracownik2) {
//    pracownik1.nazwisko() = pracownik2.nazwisko();
//    pracownik1.liczba_lat() = pracownik2.liczba_lat();
//
//}


using namespace std;

int main() {
  osoba os("Dolas", 26);
  os.pokaz();
  const pracownik pr1("Dyzma", 35, "mistrz", 1250.0);
  cout << pr1.nazwisko() << pr1.liczba_lat();
  cout << pr1.stanowisko() << pr1.placa();
  pracownik pr2(pr1);
  pr2.pokaz();
  pracownik pr3("Kos", 45, "kierownik", 2260.0);
  pr3.pokaz();
  pr3 = pr2;
  pr3.pokaz();
  osoba *w = &os;
  w->pokaz();
  w = &pr3;
  w->pokaz();
  static_cast<pracownik *>(w)->pokaz();
}
