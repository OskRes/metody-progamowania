#include <iostream>
#include <string>

using namespace std;

class prostokat
{
  protected:
    int _a;
    int _b;
    string *kolor;
  public:
    prostokat():_a(0), _b(0), kolor(new string("Brak")){}
    prostokat(int a1, int a2, const string &a3):
              _a(a1), _b(a2), kolor(new string(a3)){}
    prostokat(const prostokat &p):
              _a(p._a), _b(p._b), kolor(new string(*p.kolor)){}

    prostokat &operator=(const prostokat &p)
    {
      if(this!=&p)
      {
        _a=p._a;
        _b=p._b;
        *kolor=*p.kolor;
      }
      return *this;
    }

    int &boka(){return _a;}
    int &bokb(){return _b;}
    //const int &boka()const{return _a;}
    //const int &bokb()const{return _b;}
    //string &kolor(){return *kolor;}
    //const string &kolor()const{return *kolor;}


    friend ostream &operator<<(ostream &out, const prostokat &p)
    {
       return p.pokaz(out);
    }

    virtual ostream &pokaz(ostream &out)const
    {
       out << "Bok a: " << _a << endl;
       out << "Bok b: " << _b << endl;
       out << "kolor: " << *kolor << endl;
       return out;
    }


};

class graniastoslup:public prostokat
{
  private:
    int h;
  public:
    graniastoslup():prostokat(), h(0){}
    graniastoslup(int b1, int b2, const string &b3, int b4):
                  prostokat(b1,b2,b3), h(b4){}
    graniastoslup(const graniastoslup &g):prostokat(g), h(g.h){}
    graniastoslup &operator=(const graniastoslup &g)
    {
      if(this!=&g)
      {
         prostokat::operator=(g);
         h=g.h;
      }
      return *this;
    }

    friend ostream &operator<<(ostream &out, const graniastoslup &g)
    {
       return g.pokaz(out);
    }
};

int main()
{
/*
  //...
  prostokat p1("blue", 2, 2);
  graniastoslup g1("red", 1, 1, 1);

  //...
  tab[0]=new prostokat(p1);
  tab[1]=new graniastoslup(g1);
  tab[2]=new prostokat;
  tab[3]=new graniastoslup;
  tab[4]=new prostokat("green", 1, 2);

  for(int i=0; i<5; ++i)
      cout << *tab[i] << tab[i]->pole();
  for(int i=0; i<5; ++i)
      cout << *tab[i]=*tab[i]*2;
  for(int i=0; i<5; ++i)
      cout << *tab[i] << tab[i]->pole();
  //...
*/
  return 0;
}
