#include <iostream>
#include <string>

using namespace std;

class figura
{
protected:
    string *kolor;
public:
    figura():kolor(new string("brak")){}
    figura(const string &a1):kolor(new string(a1)){}
    figura(const figura &r):kolor(new string(*r.kolor)){}
    figura &operator=(const figura &r)
    {
        if(this!=&r)
        {
            *r.kolor=*kolor;
        }
        return *this;
    }

    virtual ostream &wypisz(ostream &out)
    {
        out << *kolor << endl;
        return out;
    }

    friend ostream &operator<<(ostream &out, figura &f);

    virtual const double &pole()const=0;
    virtual const double &objetosc()const=0;

    virtual ~figura(){delete kolor; cout << "Destruktor klasy figura" << endl; }
};
ostream &operator<<(ostream &out, figura &f)
{
    return f.wypisz(out);
}

class kwadrat: public figura
{
protected:
    double a;
    double b;
public:
    kwadrat():a(0.0), b(0.0){}
    kwadrat(const string &b1, const double &b2, const double &b3):figura(b1), a(b2), b(b3){}
    kwadrat(const kwadrat &r):figura(*r.kolor), a(r.a), b(r.b){}
    kwadrat &operator=(const kwadrat &r)
    {
        if(this!=&r)
        {
            *kolor=*r.kolor;
            a=r.a;
            b=r.b;
        }
        return *this;
    }

    const double &A()const{return a;}
    const double &B()const{return b;}

    const double &pole()const{a*b;}

    virtual ostream &wypisz(ostream &out)
    {
        out << *kolor << endl;
        out << a << " " << b << endl;
        return out;
    }

    friend ostream &operator<<(ostream &out, kwadrat &k);

    virtual ~kwadrat(){cout << "Destruktor klasy kwadrat" << endl;}
};
ostream &operator<<(ostream &out, kwadrat &k)
{
    return k.wypisz(out);
}

class prostop:public kwadrat
{
    double h;
public:
    prostop():h(0.0){}
    prostop(const string &c1, const double &c2, const double &c3, const double &c4)
        :kwadrat(c1, c2, c3), h(c4){}
    prostop(const prostop &r):kwadrat(*r.kolor, r.a, r.b), h(r.h){}
    prostop &operator=(const prostop &r)
    {
        if(this!=&r)
        {
            *kolor=*r.kolor;
            a=r.a;
            b=r.b;
            h=r.h;
        }
        return *this;
    }

    const double &H()const{return h;}

    const double &objetosc()const{a*b*h;}

    virtual ostream &wypisz(ostream &out)
    {
        out << *kolor << endl;
        out << a << " " << b << h << endl;
        return out;
    }

    friend ostream &operator<<(ostream &out, prostop &p);

    ~prostop(){cout << "Destruktor klasy prostop" << endl;}
};
ostream &operator<<(ostream &out, prostop &p)
{
    return p.wypisz(out);
}


using namespace std;

int main()
{
    return 0;
}
