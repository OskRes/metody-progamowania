#include <iostream>
#include <string>

using namespace std;

class osoba
{
protected:
   string imie;
   string nazwisko;
   int wiek;
   double wzrost;
   string *ulica;

   public:
       osoba():imie("brak"), nazwisko("brak"), wiek(0), wzrost(0.0), ulica(new string("brak")){}
       osoba(string a1, string a2, int a3, double a4, string & a5):imie(a1), nazwisko(a2), wiek(a3), wzrost(a4), ulica(new string(a5)){}
       osoba(const osoba &r):imie(r.imie), nazwisko(r.nazwisko), wiek(r.wiek), wzrost(r.wzrost), ulica(new string(*r.ulica)){}

       const osoba & operator=(const osoba &r)
       {
           if(this!=&r)
           {
             imie=r.imie;
             nazwisko=r.nazwisko;
             wiek=r.wiek;
             wzrost=r.wzrost;
           }
           return *this;
       }

       virtual ostream &wypisz(ostream &w)
       {
           w << "Imie: " << imie << endl;
           w << "Nazwisko: " << nazwisko << endl;
           w << "Wiek: " << wiek << endl;
           w << "Wzrost: " << wzrost << endl;
           w << "Ulica: " << *ulica << endl;
           return w;
       }

       friend ostream & operator <<(ostream &out, osoba &k);

       virtual ~osoba(){delete ulica;}
};

ostream &operator <<(ostream &out, osoba &k)
{
   return k.wypisz(out);
};

class adres:public osoba{
    int nrdomu;
    string miasto;

    public:
      adres():nrdomu(0), miasto("brak"){}
      adres(string b1, string b2, int b3, double b4, string &b5, int b6, string b7):osoba(b1,b2,b3,b4,b5), nrdomu(b6), miasto(b7){}
      adres(const adres &x):osoba(x.imie, x.nazwisko, x.wiek, x. wzrost, *x.ulica), nrdomu(x.nrdomu), miasto(x.miasto){}

      virtual adres &operator=(const adres &x)
      {
          if(this!=&x)
          {
              imie=x.imie;
              nazwisko=x.nazwisko;
              wiek=x.wiek;
              wzrost=x.wzrost;
              *ulica=*x.ulica;
              nrdomu=x.nrdomu;
              miasto=x.miasto;
          }
          return *this;
      }

      ostream &wypisz(ostream &k)
      {
           k << "Imie: " << imie << endl;
           k << "Nazwisko: " << nazwisko << endl;
           k << "Wiek: " << wiek << endl;
           k << "Wzrost: " << wzrost << endl;
           k << "Ulica: " << *ulica << endl;
           k << "Nr domu: " << nrdomu << endl;
           k << "Miasto: " << miasto << endl;
           return k;
      };

      friend ostream & operator << (ostream & out, adres& z);

      ~adres(){}

};

ostream &operator<<(ostream & out, adres & z)
{
    return z.wypisz(out);
}

int main()
{
    return 0;
}
