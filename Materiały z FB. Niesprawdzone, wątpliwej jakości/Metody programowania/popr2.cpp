#include <iostream>
#include <string>

using namespace std;

class B
{
    public:
        virtual ostream &pokaz(ostream & out)const=0;
        virtual ~B(){cout << "Destruktor B" << endl;}
};

class P1:public B
{
    private:
        string *t;
    public:
        P1():t(new string[2]){t[0]="Brak"; t[1]="Brak";}
        P1(const string &a1, const string &a2):t(new string[2]){t[0]=a1; t[1]=a2;}
        P1(const P1 &p):t(new string(*p.t))
        {
            t[0]=p.t[0];
            t[1]=p.t[1];
        }
        P1 &operator=(const P1 &p)
        {
            if(this!=&p)
            {
                delete [] t;
                t=new string [2];
                t[0]=p.t[0];
                t[1]=p.t[1];
            }
            return *this;
        }

        string &a1(){return t[0];}
        string &a2(){return t[1];}
        const string &a1()const{return t[0];}
        const string &a2()const{return t[1];}

        friend ostream &operator<<(ostream &out, const B &p);
        virtual ostream &pokaz(ostream &p)
        {
            p << t[0] << " ";
            p << t[1] << " ";
            return p;
        }

        ~P1(){delete [] t; cout << "Destruktor P1" << endl;}
};

class P2:public B
{
    private:
        P1 x;
    public:
        P2():x(){}
        P2(const string &b1, const string &b2):x(b1,b2){}
        P2(const P2 &p):x(p.x){}
        P2 &operator=(const P2 &p)
        {
            if(this!=&p)
            {
                x=p.x;
            }
            return *this;
        }

        P1 &metx(){return x;}

       // friend ostream &operator <<(ostream &out, const P2 &p);
        virtual ostream & pokaz(ostream &p)const
        {
            p << x.a1() << endl;
            p << x.a2() << endl;
            return p;
        }

        ~P2(){cout << "Destruktor P2" << endl;}

};

int main()
{

	P1 w1;
    P1 w2("Ala","Koala");
	P2 w3;
    P2 w4("Warszawa","Krakow");
    P1 w5("******","-------");

	w1=w5;
//	w3=w5;
	B *tab[4];

	tab[0]=&w1;
	tab[1]=&w2;
	tab[2]=&w3;
	tab[3]=&w4;

	for(int i=0;i<4;++i){
		cout<<tab[i]<<endl;
	}

	return 0;
}
