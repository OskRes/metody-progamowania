#include <iostream>
#include <string>

using namespace std;

class osoba
{
public:
    string nazwisko;
    int wiek;

    osoba():nazwisko("Brak"), wiek(0){}
    osoba(const string &a1, const int &a2): nazwisko(a1), wiek(a2){}
    osoba(const osoba &o): nazwisko(o.nazwisko), wiek (o.wiek){}
    osoba &operator =(const osoba &o){
        if(this!=&o)
        {
            nazwisko=o.nazwisko;
            wiek=o.wiek;
        }
        return *this;
    }

    friend ostream &operator << (ostream &out, const osoba &o);
    virtual void pokaz (ostream &out);
    virtual ~osoba(){}
};

ostream &operator << (ostream &out, const osoba &o)
{
    o.pokaz(out);
    return out;
}

int main()
{
    osoba os("Dolas", 26);
    os.pokaz();
/*    const pracownik pr1("Dyzma", 35, "mistrz", 1250.0);
    cout << pr1.nazwisko() << pr1.liczba_lat();
    cout << pr1.stanowisko() << pr1.placa();
    pracownik pr2(pr1);
    pr2.pokaz();
    pracownik pr3("Kos", 45, "kierownik", 2260.0);
    pr3.pokaz();
    pr3 = pr2;
    pr3.pokaz();
    osoba* w = &os;
    w->pokaz();
    w = &pr3;
    w->pokaz();
    static_cast<pracownik*>(w)->pokaz();
*/
}
