#include <iostream>
#include <string>

using namespace std;

class osoba
{
protected:
    string nazwisko;
    int wiek;

public:
    osoba():nazwisko("brak"), wiek(0){}
    osoba(const string &a1, const int &a2):nazwisko(a1), wiek(a2){}
    osoba(const osoba &o):nazwisko(o.nazwisko), wiek(o.wiek){}
    osoba &operator=(const osoba &o)
    {
        if(this!=&o)
        {
            nazwisko=o.nazwisko;
            wiek=o.wiek;
        }
        return *this;
    }

    const string &nazwiskop()const{return nazwisko;}
    const int &wiekp()const{return wiek;}
    string &nazwiskop(){return nazwisko;}
    int &wiekp(){return wiek;}

    friend ostream &operator<<(ostream &out, osoba &o);
    virtual void pokaz()
    {
        cout << "Nazwisko: " << nazwisko << endl;
        cout << "Wiek: " << wiek << endl;
    }

    virtual ~osoba(){cout << "Destruktor osoba" << endl;}
};

class pracownik:public osoba
{
private:
    string stanowisko;
    double placa;
public:
    pracownik():osoba("Brak", 0), stanowisko("Brak"), placa(0.0){}
    pracownik(const string &b1, const int &b2, const string &b3, const double &b4):
        osoba(b1,b2), stanowisko(b3), placa(b4){}
    pracownik(const pracownik &p):osoba(p.nazwisko, p.wiek), stanowisko(p.stanowisko), placa(p.placa){}
    pracownik &operator=(const pracownik &p)
    {
        if(this!=&p)
        {
            nazwisko=p.nazwisko;
            wiek=p.wiek;
            stanowisko=p.stanowisko;
            placa=p.placa;
        }
        return *this;
    }

    string &stanowiskop(){return stanowisko;};
    double &placap(){return placa;}
    const string &stanowiskop()const{return stanowisko;};
    const double &placap()const{return placa;}

    friend ostream &operator <<(ostream &out, pracownik &p);
    void pokaz()
    {
        cout << "Nazwisko: " << nazwisko << endl;
        cout << "Wiek: " << wiek << endl;
        cout << "Stanowisko: " << stanowisko << endl;
        cout << "Placa: " << placa << endl;
    }

    ~pracownik(){cout << "Destruktor pracownik" << endl;}
};

int main()
{

    osoba os("Dolas", 26);
    os.pokaz();

    const pracownik pr1("Dyzma", 35, "mistrz", 1250.0);
    cout << pr1.nazwiskop() << pr1.wiekp();
    cout << pr1.stanowiskop() << pr1.placap();
    pracownik pr2(pr1);
    pr2.pokaz();
    pracownik pr3("Kos", 45, "kierownik", 2260.0);
    pr3.pokaz();
    pr3 = pr2;
    pr3.pokaz();
    osoba* w = &os;
    w->pokaz();
    w = &pr3;
    w->pokaz();
    static_cast<pracownik*>(w)->pokaz();

    return 0;
}
