#include <iostream>

using namespace std;

class punkt{
	private:
		double a, b, c;	
	public:
		punkt() { a = 0; b = 0; c = 0; }; 
		punkt(int x, int y, int z);
		punkt(double x, double y, double z);
		double & x(){  return this->a; };
		double & y(){  return this->b; };
		double & z(){  return this->c; };
		double x() const { return this->a; };
		double y() const { return this->b; };
		double z() const { return this->c; };
};

punkt::punkt(int x, int y, int z){
	this->a = x;
	this->b = y;
	this->c = z;
}

punkt::punkt(double x, double y, double z){
	this->a = x;
	this->b = y;
	this->c = z;
}

class prostokat{
	private: 
		double x1, y1, z1, a1, b1;
	public:
		prostokat(){ x1 = 0; y1 = 0; z1 = 0; a1 = 0; b1 = 0; };
		prostokat(double a, double b, double c, double d, double e);
		prostokat(punkt & p, double q, double w){
			this->x1 = p.x();
			this->y1 = p.y();
			this->z1 = p.z();
			this->a1 = q;
			this->b1 = w;
		}
		double & x(){  return this->x1; };
		double & y(){  return this->y1; };
		double & z(){  return this->z1; };
		double & a(){  return this->a1; };
		double & b(){  return this->b1; };
		double x() const { return this->x1; };
		double y() const { return this->y1; };
		double z() const { return this->z1; };
		double a() const { return this->a1; };
		double b() const { return this->b1; };
		double pole() const;
		double pole();
};

prostokat::prostokat(double a, double b, double c, double d, double e){
	this->x1 = a;
	this->y1 = b;
	this->z1 = c;
	this->a1 = d;
	this->b1 = e;
}

double prostokat::pole() const{
	return this->a1*this->b1;
}

double prostokat::pole(){
	return this->a1*this->b1;
}

class graniastoslup{
	private:
		double x1, y1, z1, a1, b1, h1;
	public:
		graniastoslup(){ x1 = 0; y1 = 0; z1 = 0; a1 = 0; b1 = 0; h1 = 0; };
		graniastoslup(double x, double y, double z, double a, double b, double h);
		graniastoslup(punkt & p, int a, int b, int h);
		graniastoslup(const prostokat & pr, int h);
		double & x(){  return this->x1; };
		double & y(){  return this->y1; };
		double & z(){  return this->z1; };
		double & a(){  return this->a1; };
		double & b(){  return this->b1; };
		double & h(){  return this->h1; };
		double x() const { return this->x1; };
		double y() const { return this->y1; };
		double z() const { return this->z1; };
		double a() const { return this->a1; };
		double b() const { return this->b1; };
		double h() const { return this->h1; };
		double objetosc() const;
		double objetosc();
};

graniastoslup::graniastoslup(double x, double y, double z, double a, double b, double h){
	this->x1 = x;
	this->y1 = y;
	this->z1 = z;
	this->a1 = a;
	this->b1 = b;
	this->h1 = h;
}

graniastoslup::graniastoslup(punkt & p, int a, int b, int h){
	this->x1 = p.x();
	this->y1 = p.y();
	this->z1 = p.z();
	this->a1 = a;
	this->b1 = b;
	this->h1 = h;
}

graniastoslup::graniastoslup(const prostokat & pr, int h){
	this->x1 = pr.x();
	this->y1 = pr.y();
	this->z1 = pr.z();
	this->a1 = pr.a();
	this->b1 = pr.b();
	this->h1 = h;
}

double graniastoslup::objetosc() const{
	return this->a1*this->b1*this->h1;
}

double graniastoslup::objetosc(){
	return this->a1*this->b1*this->h1;
}



int main(){

	punkt p1, p2(1,2,3); 
	const punkt p3(1.1,2.2,3.3); 
 
	cout << p3.x() << "\t" << p3.y() << "\t" << p3.z() << endl; 
	p1.x()=1; 
	p1.y()=10; 
	p1.x()=100; 
	cout << p1.x() << "\t" << p1.y() << "\t" << p1.z() << endl; 
	
	cout<<endl;
	cout<<endl;
	
	prostokat pr1, pr2(1,2,3,10.5, 20.5); 
	const prostokat pr3(p2,5,5); 
	 
	cout 
	 << pr3.x() << "\t" << pr3.y() << "\t" << pr3.z() << "\n" 
	 << pr3.a() << "\t" << pr3.b()<< "\n" 
	 << pr3.pole() << endl; 
	 
	pr1.x()=2; 
	pr1.y()=20; 
	pr1.x()=200; 
	pr1.a()= 10; 
	pr1.b()=10; 
	
	cout 
	 << pr1.x() << "\t" << pr1.y() << "\t" << pr1.z() << "\n" 
	 << pr1.a() << "\t" << pr1.b()<< "\n"  
	 << pr1.pole() << endl; 

	cout<<endl;
	cout<<endl;

	graniastoslup g1, g2(1,2,3,10.5,20.5,30.5), g3(p2,100,200,300); 
	const graniastoslup g4(pr3,5); 
	 
	cout 
	 << g4.x() << "\t" << g4.y() << "\t" << g4.z() << "\n" 
	 << g4.a() << "\t" << g4.b() << "\t" << g4.h() << "\n" 
	 << g4.objetosc() << endl; 
	 
	g1.a()=10; 
	g1.b()=10; 
	g1.h()=10; 
	
	cout 
	 << g1.x() << "\t" << g1.y() << "\t" << g1.z() << "\n" 
	 << g1.a() << "\t" << g1.b() << "\t" << g1.h() << "\n" 
	 << g1.objetosc() << endl; 

	return 0;
}
