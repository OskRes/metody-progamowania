#include <iostream>
#include <string>

using namespace std;

class pojazd
{
protected:
    string nr_rejestracyjny;
    int liczba_pasazerow;
  public:
    pojazd():nr_rejestracyjny("brak"), liczba_pasazerow(0){}
    pojazd(string a1, int a2):nr_rejestracyjny(a1), liczba_pasazerow(a2){}
    pojazd(const pojazd &x):nr_rejestracyjny(x.nr_rejestracyjny), liczba_pasazerow(x.liczba_pasazerow){}
    pojazd &operator=(const pojazd &x)
    {
       if(this!=&x)
       {
        nr_rejestracyjny=x.nr_rejestracyjny;
        liczba_pasazerow=x.liczba_pasazerow;
       }
       return *this;
    }

    const string &m1()const{return nr_rejestracyjny;}
    const int &m2()const {return liczba_pasazerow;}

    virtual ostream &wyswietl(ostream &w)
    {
        w << "Nr rejestracyjny: " << nr_rejestracyjny << endl;
        w << "Liczba pasazerow: " << liczba_pasazerow << endl;
        return w;
    }

    friend ostream &operator <<(ostream &out, pojazd &p);

    virtual ~pojazd(){}
};
ostream &operator <<(ostream &out, pojazd &p)
{
    return p.wyswietl(out);
}

class autobus:public pojazd
{
    string nazwisko_kierowcy;
    int *nr_linii;
   public:
    autobus():pojazd("brak", 0), nazwisko_kierowcy("brak"), nr_linii(0){}
    autobus(string b1, int b2, string b3, int b4):pojazd(b1, b2), nazwisko_kierowcy(b3), nr_linii(new int(b4)){}
    autobus(const autobus &r):pojazd(r.nr_rejestracyjny, r.liczba_pasazerow), nazwisko_kierowcy(r.nazwisko_kierowcy), nr_linii(new int(*r.nr_linii)){}
    autobus &operator=(const autobus &r)
    {
      if(this!=&r)
      {
        nr_rejestracyjny=r.nr_rejestracyjny;
        liczba_pasazerow=r.liczba_pasazerow;
        nazwisko_kierowcy=r.nazwisko_kierowcy;
        *nr_linii=*r.nr_linii;
      }
      return *this;
    }

    const string &m3()const{return nazwisko_kierowcy;}
    const int &m4()const{return *nr_linii;}

    virtual ostream &wyswietl(ostream &z)
    {
        z << "Nr rejestracyjny: " << nr_rejestracyjny << endl;
        z << "Liczba pasazerow: " << liczba_pasazerow << endl;
        z << "Nazwisko kierowcy: " << nazwisko_kierowcy << endl;
        z << "Nr linii: " << *nr_linii << endl;
        return z;
    }

    friend ostream &operator<<(ostream &out, autobus &z);

    ~autobus(){delete nr_linii;}
};
ostream &operator<<(ostream &out, autobus &z)
{
    return z.wyswietl(out);
}


int main()
{
    pojazd p1("SC 12345", 20);
    cout << p1.m1() << p1.m2() << endl;

    autobus a1("SC 99999", 20, "Kowalski", 24);
    cout << a1.m3() << a1.m4();
    cout << a1.m1() << a1.m2() << endl;

    autobus a2(a1);
    a2.wyswietl();

    autobus a3("SC 11111", 23, "Jankowski", 16);
    a3.wyswietl();
    a3=a2;
    a3.wyswietl();

    pojazd *p2=&p1;
    p2->wyswietl();
    p2=&a3;
    p2->wyswietl();

    return 0;
}
