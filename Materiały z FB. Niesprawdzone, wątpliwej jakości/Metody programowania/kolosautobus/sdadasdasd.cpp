#include <iostream>
#include <string>

using namespace std;

class pojazd
{
    protected:
        string nr_rej;
        int il_pas;
    public:
        pojazd():nr_rej("Brak"), il_pas(0){}
        pojazd(const string &a1, const int &a2):nr_rej(a1), il_pas(a2){}
        pojazd(const pojazd &p): nr_rej(p.nr_rej), il_pas(p.il_pas){}
        pojazd &operator=(const pojazd &p)
        {
            if(this!=&p)
            {
                nr_rej=p.nr_rej;
                il_pas=p.il_pas;
            }
            return *this;
        }

        const string &m1()const{return nr_rej;}
        const int &m2()const{return il_pas;}

        friend ostream &operator<<(ostream &out, pojazd &p);
        ostream &wyswietl()
        {
            cout << "Nr rej: " << nr_rej << endl;
            cout << "Ilosc pasazerow: " << il_pas << endl;
            //return out;
        }

        virtual ~pojazd(){}
};
/*
ostream &operator <<(ostream &out, pojazd &p)
{
    return p.wyswietl(out);
}
*/



class autobus:public pojazd
{
    private:
        string naz_kier;
        int *nr_linii;
    public:
        autobus():pojazd("Brak", 0), naz_kier("Brak"), nr_linii(0){}
        autobus(string b1, int b2, string b3, int b4):pojazd(b1, b2), naz_kier(b3), nr_linii(new int(b4)){}
        autobus(const autobus &a):pojazd(a.nr_rej, a.il_pas), naz_kier(a.naz_kier), nr_linii(new int(*a.nr_linii)){}
        autobus &operator =(const autobus &a)
        {
            if(this!=&a)
            {
                nr_rej=a.nr_rej;
                il_pas=a.il_pas;
                naz_kier=a.naz_kier;
                *nr_linii=*a.nr_linii;
            }
            return *this;
        }

        const string &m3()const{return naz_kier;}
        const int &m4()const{return *nr_linii;}

        friend ostream &operator<<(ostream &out, autobus &a);
        virtual ostream &wyswietl()
        {
            cout << "Nr rej: " << nr_rej << endl;
            cout << "Ilosc pasazerow: " << il_pas << endl;
            cout << "Nazwisko kierowcy: " << naz_kier << endl;
            cout << "Nr linii: " << *nr_linii << endl;
           // return out;
        }

        ~autobus(){delete nr_linii;}
};
/*
ostream &operator<<(ostream &out, autobus &a)
{
    return a.wyswietl(out);
}
*/

int main()
{

    pojazd p1("SC 12345", 20);
    cout << p1.m1() << p1.m2() << endl;

    autobus a1("SC 99999", 20, "Kowalski", 24);
    cout << a1.m3() << a1.m4();
    cout << a1.m1() << a1.m2() << endl;

    autobus a2(a1);
    a2.wyswietl();

    autobus a3("SC 11111", 23, "Jankowski", 16);
    a3.wyswietl();
    a3=a2;
    a3.wyswietl();

    pojazd *p2=&p1;
    p2->wyswietl();
    p2=&a3;
    p2->wyswietl();

    return 0;
}
