//Michał Kowacki
#include <iostream>
#include <cmath>
using namespace std;

class point{
	public:
		double * tab;
	public:
		point() { tab = new double[3]; }
		point(double * t);
		point(double a, double b, double c);
		point(point a, point b, point c);
		~point(){};
		double * t(){ return this->tab; }
		double & operator[](size_t el) { return this->tab[el]; }
		double & operator[](size_t el) const{ return this->tab[el]; }
		double distance(point p);
		double distance(point p) const;
		point operator +(point & p);
		point operator -(const point & p);
		bool operator <(point p);
		friend ostream & operator<< (ostream & o, point & p);
		friend ostream & operator<< (ostream & o, const point & p);
};

double point::distance(const point p){
	return sqrt(pow(this->tab[0]-p[0], 2)+pow(this->tab[2]-p[2],2)+pow(this->tab[3]-p[3], 2));
}

double point::distance(const point p) const{
	return sqrt(pow(this->tab[0]-p[0], 2)+pow(this->tab[2]-p[2],2)+pow(this->tab[3]-p[3], 2));
}

point::point(double a, double b, double c){
	this->tab = new double[3];
	this->tab[0] = a;
	this->tab[1] = b;
	this->tab[2] = c;
}

point::point(double * t){
	this->tab = t;
}

point point::operator +(point & p){
	cout<<this->tab[0]+p[0];
	point q(this->tab[0]+p[0],this->tab[1]+p[1],this->tab[2]+p[2]);
	return q;
}
point point::operator -(const point & p){
	point q(this->tab[0]-p[0],this->tab[1]-p[1],this->tab[2]-p[2]);
	return q;
}

point operator *(float a, point & p){
	point pq(p[0]*a,p[1]*a,p[2]*a);
	return pq;
}

point operator *(point & p, float a){
	point pq(p[0]*a,p[1]*a,p[2]*a);
	return pq;
}

bool point::operator<(point p){
	point s(0,0,0);
	if(s.distance(p) < s.distance(this->tab)){
		return true;
	} else {
		return false;
	}
}

bool operator ==(point p, point p1){
	point s(0,0,0);
	double eps = pow(10, -10);
	if(fabs(s.distance(p) == s.distance(p1))<eps){
		return true;
	} else {
		return false;
	}
}

ostream & operator<<(ostream & os, point & v){
	return os << v[0] << " " << v[1] << " " << v[2];
}
ostream & operator<<(ostream & os, const point & v){
	return os << v[0] << " " << v[1] << " " << v[2];
}

istream & operator>>(istream & os, point & p){
	return os >> p[0] >> p[1] >> p[2];
}

int main(){

	double x[2][3] = {{1.0, 1.0, 1.0}, {1.0, 2.0, 3.0}}; 

	point p1(x[0]), p2(x[1]); 
	const point p3(0.4, 0.2, 0.1); 
	 
	cout << p1 << ", " << p2 << '\n'; 
	cout << p3[0] << ' ' << p3[1] << ' ' << p3[2] << '\n'; 
	cout << p1.distance(point()) << ", "<< p3.distance(p1) << '\n'; 
	 
	cout << p1 + p2 << ", " << p1 - p3 << '\n'; 
	cout << 3.14 * p2 << ", " << p2 * 3.14 << '\n'; 
	cout << (p1 < p3) << ", " << (p1 == point(1.0, 1.0, 1.0)) << '\n';
	cin >> p1; 
	cout << p1 << '\n';

	return 0;
}
