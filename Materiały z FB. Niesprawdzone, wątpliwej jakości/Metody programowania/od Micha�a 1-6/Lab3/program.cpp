//Michał Kowacki
#include <iostream>
#include <cmath>
using namespace std;

class punkt{
	private: 
		double x1, y1;
	public:
		punkt() { x1 = 0; y1 = 0; };
		punkt(double x, double y){
			this->x1 = x;
			this->y1 = y;
		}
	double & x(){ return this->x1; }
	double & y(){ return this->y1; }
	double odleglosc(punkt);
};

double punkt::odleglosc(punkt p){
	return sqrt(pow(this->x()-p.x(),2)+pow(this->y()-p.y(),2));
}

class wielobok{
	private:
		int liczba;
		punkt * Punkt1;
	public:
		wielobok(){ liczba = 0; }
		wielobok(punkt * t, punkt * t1);
		~wielobok(){ delete [] Punkt1; }
		int & ilosc(){ return this->liczba; }
		punkt & Punkt(int a){ return this->Punkt1[a]; }
		void Punkty(punkt * t, punkt * t1);
		double obwod();
};

void wielobok::Punkty(punkt * t, punkt * t1){	
	this->Punkt1 = new punkt[3];
	unsigned int i = 0;
	while(t < t1){
		this->Punkt1[i] = t[0];
		t++;
		i++;
	} 
	this->liczba = i;
}

double wielobok::obwod(){
	double obwod = 0;
	for(int i = 0; i<=this->liczba-1; i++){
		if(i == this->liczba-1){
			obwod += Punkt1[0].odleglosc(Punkt1[i]);
		} else {
			obwod += Punkt1[i].odleglosc(Punkt1[i+1]);
		}
	}
	return obwod;
}

wielobok::wielobok(punkt * t, punkt * t1){
	this->Punkt1 = new punkt[4];
	unsigned int i = 0;
	while(t < t1){
		this->Punkt1[i] = t[0];
		t++;
		i++;
	} 
	this->liczba = i;
}

int main(){
	 	 
	punkt p(2, 3); 
	cout << p.x() << ' ' << p.y() << '\n'; 
	p.x() = 1; 
	p.y() = 1;
	cout << p.x() << ' ' << p.y() << '\n';
	cout << p.odleglosc(punkt()) << '\n'; 
	
	punkt t[] = { punkt(0, 1), punkt(0, 0), punkt(1, 0), punkt(1, 1) }; 
	
	wielobok w1(t, t+4);
	 
	cout << w1.obwod() << '\n'; 
 	w1.Punkt(1) = punkt(0.5, 0.5); 
	cout << w1.obwod() << '\n'; 
	 
	wielobok w2; 
	w2.Punkty(t, t+3); 
	cout << w2.obwod() << '\n'; 
	for (int i = 0; i < w2.ilosc(); ++i) 
	cout << w2.Punkt(i).x() << ' ' << w2.Punkt(i).y() << '\n'; 

	return 0;
}
