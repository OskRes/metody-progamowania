//Michał Kowacki
#include <iostream>
#include <string>

using namespace std;

class adres{
	private:
		string miasto1, kod1, ulica1;
		unsigned int numer1;
	public:
		adres(){ miasto1 = ""; kod1 = ""; ulica1 = ""; numer1 = 0; }
		adres(adres *wsk);
		adres(string miasto, string kod, string ulica, unsigned int numer);
		~adres(){ };
		string miasto(){ return this->miasto1; }
		string kod() { return this->kod1; }
		string ulica() { return this->ulica1; }
		unsigned int numer() { return this->numer1; }
		
		string miasto() const{ return this->miasto1; }
		string kod() const{ return this->kod1; }
		string ulica() const{ return this->ulica1; }
		unsigned int numer() const{ return this->numer1; }
};

std::ostream & operator<<(std::ostream& os, adres & v){
	return os << v.miasto() << " " << v.kod() << " " << v.ulica() << " " << v.numer();
}

std::ostream & operator<<(std::ostream& os, const adres & v){
	return os << v.miasto() << " " << v.kod() << " " << v.ulica() << " " << v.numer();
}

adres::adres(adres *wsk){
	this->miasto1 = wsk[0].miasto1;
	this->kod1 = wsk[0].kod1;
	this->ulica1 = wsk[0].ulica1;
	this->numer1 = wsk[0].numer1;
}

adres::adres(string miasto, string kod, string ulica, unsigned int numer){
	this->miasto1 = miasto;
	this->kod1 = kod;
	this->ulica1 = ulica;
	this->numer1 = numer;
}

class osoba{
		private: 
			string imie1, nazwisko1;
			unsigned int wiek1;
			adres a1;
		public:
			osoba(){ imie1 = ""; nazwisko1 = ""; wiek1 = 0; }
			osoba(string imie, string nazwisko, unsigned int wiek, const adres a);
			string imie() { return this->imie1; }
			string nazwisko() { return this->nazwisko1; }
			unsigned int wiek(){ return this->wiek1; }
			adres a() { return this->a1; }
};


osoba::osoba(string imie, string nazwisko, unsigned int wiek, const adres a){
		this->imie1 = imie;
		this->nazwisko1 = nazwisko;
		this->wiek1 = wiek;
		this->a1 = a; 
}

std::ostream & operator<<(std::ostream& os, osoba & v){
	return os << v.imie() << " " << v.nazwisko() << " " << v.wiek() << " " << v.a();
}

int main(){

	adres* wsk = new adres("Czestochowa", "42-200", "Dabrowskiego", 73); 
	cout<<*wsk<< "\n"; 
	adres a1(*wsk); 
	delete wsk; 

	const adres* wsk1 = new adres("Warszawa", "00-950", "Mysliwiecka", 357); 
	cout << a1 << '\n'; 
	cout << *wsk1 << '\n'; 
	adres a2; 
	cout << a2 << '\n'; 
	a2 = a1; 
	cout << a2 << '\n'; 
	 
	osoba o("Jan", "Kos", 25, *wsk1); 
	 
	delete wsk1; 
	 
	cout << o << '\n'; 
	osoba o1(o); 
	cout << o1 << '\n'; 
	osoba o2; 
	cout << o2 << '\n'; 
	o2 = o1; 
	cout << o2 << '\n'; 
	
	return 0;
}
