//Michał Kowacki
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

using namespace std;

class wielokat{
	private:
		unsigned int rozmiar;
		double ** tab;
		ifstream plik;
		void wczytaj();
		void wyswietl();
	public:
		wielokat(){  }
		wielokat(string nazwa);
		~wielokat();
		void open(string nazwa);
		double obwod();
		double pole();
};

double wielokat::pole(){
	double pole = 0;
	for(unsigned int i = 0; i<=rozmiar-1; i++){
		if(i == rozmiar-1){
			pole += (tab[i][0]*tab[0][1])-(tab[i][1]*tab[0][0]);
		} else {
			pole += (tab[i][0]*tab[i+1][1])-(tab[i][1]*tab[i+1][0]);
		}
	}
	return pole/2;
}

double wielokat::obwod(){
	double obwod = 0;
	for(unsigned int i = 0; i<=rozmiar-1; i++){
		if(i == rozmiar-1){
			obwod += sqrt(pow(tab[i][0]-tab[0][0],2)+pow(tab[i][1]-tab[0][1],2));
		} else {
			obwod += sqrt(pow(tab[i][0]-tab[i+1][0],2)+pow(tab[i+1][1]-tab[i][1],2));
		}
	}
	return obwod;
}

void wielokat::wyswietl(){
	for(unsigned int i = 0; i<this->rozmiar; i++){
		for(unsigned int j = 0; j<2; j++){
			cout<<this->tab[i][j]<<" ";
		}
		cout<<"\n";
	}
}

void wielokat::wczytaj(){
	unsigned int index = 0;
	plik.seekg( 30, ios_base::beg );
	plik>>this->rozmiar;
	plik.seekg( 9, ios::cur );
	this->tab = new double*[this->rozmiar];
	for(unsigned int i = 0; i<this->rozmiar; i++){ // wczytanie wartości [NODES] do tablicy
		plik>>index;
		if(index != i+1){
			this->tab[index-1] = new double[2];
			for(unsigned int j = 0; j<2; j++){
				plik>>this->tab[index-1][j];
			}
		} else {
			this->tab[i] = new double[2];
			for(unsigned int j = 0; j<2; j++){
				plik>>this->tab[i][j];
			}
		}
	}
	plik.seekg(11, ios::cur); 
	for(unsigned int i = 0; i<this->rozmiar; i++){ // sortowanie w tablicy według kolejności [POLYGON]  w pliku
		plik>>index;
		if(index != i+1){
			double * temp = this->tab[i];
			tab[i] = tab[index-1];
			tab[index-1] = temp;
		} 
	}
}

wielokat::wielokat(string nazwa){
	this->plik.open(nazwa.c_str());
	if(!this->plik.good()){
		cerr<<"Blad otwarcia"<<endl;
		plik.clear();
		plik.ignore();
		plik.close();
	} 
	this->wczytaj();
//	this->wyswietl();
}

wielokat::~wielokat(){
	for(unsigned int i = 0; i<rozmiar; i++){
		delete [] tab[i];
	} 
	delete [] tab;
	plik.close();
}

void wielokat::open(string nazwa){
	this->plik.open(nazwa.c_str());
	if(!this->plik.good()){
		cerr<<"Blad otwarcia"<<endl;
		plik.clear();
		plik.ignore();
		plik.close();
	} 
	this->wczytaj();
}

int main(){
//	wielokat w;
//	w.open("plik.txt"); // otwarcie pliku przez metodę open

	wielokat w("plik.txt"); // otwarcie pliku w konstruktorze
	cout<<"Obwód figury wynosi: "<<w.obwod()<<"\n";
	cout<<"Pole figury wynosi: "<<w.pole()<<"\n";
	return 0;
}
