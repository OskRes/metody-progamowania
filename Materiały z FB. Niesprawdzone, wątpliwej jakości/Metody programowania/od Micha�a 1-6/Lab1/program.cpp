//Michał Kowacki
#include <iostream>
#include <string> 
#include <fstream>

using namespace std;

struct Student{
   string imie,nazw;
   int numer;
};

struct Lista{
   int liczba;
   Student * stud;
};

bool wczytaj(Lista & l){ 
	ifstream plik; 
	plik.open("plik.txt"); 

	if(!plik.good()){ 
	   cerr<<"Blad strumienia pikowego";
       return false;
	}

	plik>>l.liczba; 

	l.stud = new Student[l.liczba]; 

	for(int i = 0; i<=l.liczba; i++){ 
            plik>>l.stud[i].imie; 
	    plik>>l.stud[i].nazw;
	    plik>>l.stud[i].numer;
	}
	return true;
}

void wypisz(Student & stud){ 
	cout<<stud.imie<<" "<<stud.nazw<<" "<<stud.numer<<endl; 
}

void wypisz(Lista & l){ 
	for(int i = 0; i<l.liczba; i++){ 
		wypisz(l.stud[i]);
	}
}

void edytuj(Lista & l, int wybor){
	cout<<"Podaj nowe imie:"<<endl;
	cin>>l.stud[wybor].imie;
        cout<<"Podaj nowe nazwisko:"<<endl;
	cin>>l.stud[wybor].nazw;
	cout<<"Podaj nowy numer:"<<endl;
	cin>>l.stud[wybor].numer;
}

void zapisz(Lista & l){
	ofstream plik;
	plik.open("zapis.txt"); 
	
	for(int i = 0; i<l.liczba; i++){ 
		plik<<l.stud[i].imie<<" "<<l.stud[i].nazw<<" "<<l.stud[i].numer<<endl;
	}
}

int main(){

    Lista l;
    int wybor; 
    if(wczytaj(l)){ 
		cout<<"Wpisz numer studenta którego chcesz wyswietlić od 0 do "<<l.liczba<<endl;
        cin>>wybor;
		cout<<"Dane twojego studenta:"<<endl;
		wypisz(l.stud[wybor]);
		cout<<endl;
		cout<<"Lista wszystkich studentów:"<<endl;
		wypisz(l);
		cout<<"Wpisz numer studenta którego chcesz edytować od 0 do "<<l.liczba<<endl;
        cin>>wybor;
		edytuj(l, wybor);
		zapisz(l);
    } 

    return 0;
}
