#include <iostream>
#include <fstream>

using namespace std;

class osoba
{
	string *im;
	string naz;
	int wi;
	public:
	osoba(): im(new string("brak")), naz("brak"), wi(0){}
	osoba(const string& imie, const string& nazwisko, int wiek): im(new string(imie)), naz(nazwisko), wi(wiek){}
	osoba(const osoba& druga): im(new string(*druga.im)), naz(druga.naz), wi(druga.wi){}
	osoba& operator=(const osoba&);
	friend ostream& operator<<(ostream&, const osoba&);
	osoba operator+(int);
	osoba& operator+=(int);
	void read(ifstream&);
	friend osoba operator+(const osoba&, const string&);
	friend osoba operator+(const string&, const osoba&);
	~osoba(){delete im;}
};

void osoba::read(ifstream& plik)
{
		plik >> *im;
		plik >> naz;
		plik >> wi;
}

osoba& osoba::operator+=(int liczba)
{
	wi+=liczba;
	return *this;
}

osoba operator+(const string& nazwisko, const osoba& os)
{
	return osoba(*os.im, nazwisko, os.wi);
}

osoba operator+(const osoba& os, const string& nazwisko)
{
	return osoba(*os.im, nazwisko, os.wi);
}

osoba osoba::operator+(int liczba)
{
	return osoba(*im, naz, wi=wi+liczba);
}

ostream& operator<<(ostream& wyjscie, const osoba& os)
{
	wyjscie << *os.im << " " << os.naz << " " << os.wi << endl;
	return wyjscie;
}

osoba& osoba::operator=(const osoba& druga)
{
	if(this!=&druga)
	{
		im = new string(*druga.im);
		naz = druga.naz;
		wi = druga.wi;
		return *this;
	}
	return *this;
}

int main()
{
	osoba o1, o2;
	const osoba* wsk1 = new osoba("Ala", "Koala", 15);
	const osoba o3(*wsk1);
	delete wsk1;
	wsk1=0;
	const osoba* wsk2 = new osoba(o3);
	o2=*wsk2;
	cout << o1 << *wsk2;
	delete wsk2;
	wsk2=0;
	cout << o2;
	cout << o2+10;
	//*****3*****
	osoba tab[4];
	ifstream wejscie("data.txt");
	for(int i=0; i<4; i++)
	{
		tab[i].read(wejscie);
	}
	for(int i=0; i<4; i++)
	{
		tab[i]+=1;
		cout << tab[i];
	}
	//*****4*****
	tab[1]=tab[1]+"Kowalska";
	tab[3]="Bocian"+tab[3];
	for(int i=0; i<4; i++)
	{
		cout << tab[i];
	}
	//*****5*****
	return 0;
}
