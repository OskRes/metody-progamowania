#include <iostream>

using namespace std;


class figura{

public:


virtual double pole()=0;

virtual ~figura(){}
};


class kolo:public figura{

protected:
    int r;
    string *wsk;
public:
    kolo():wsk(NULL),r(0){}
    kolo(const string & a1, const int & a2):wsk(new string(a1)),r(a2){}


virtual double pole(){ return 3.14*r*r;}
};


class prostop:public figura{

protected:
    string *wsk;
    int z1;
    int z2;
    int z3;
public:
    prostop():wsk(NULL),z1(0),z2(0),z3(0){}
    prostop(const string & a1, const int & a2,const int & a3,const int & a4):wsk(new string(a1)),z1(a2),z2(a3),z3(a4){}


virtual double pole(){ return z1*z2;}
};

class prost:public figura{

protected:
    string *wsk;
    int z1;
    int z2;

public:
    prost():wsk(NULL),z1(0),z2(0){}
    prost(const string & a1, const int & a2,const int & a3):wsk(new string(a1)),z1(a2),z2(a3){}


virtual double pole(){ return z1*z2;}
};




int main()
{


  figura*tab[5];

  const kolo test1("czarny",100);

  const prostop test2("szary",2,2,2);


  tab[0]=new kolo("czerwony",1);
  tab[1]=new kolo;
  tab[2]=new prost("niebieski",1,1);
  tab[3]=new prostop("zielony",1,1,1);
  tab[4]=new prostop;


  for (int i=0;i<5;++i)
    cout<<tab[i]->pole()<<endl;



    return 0;
}
