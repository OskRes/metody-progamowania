/*
	 Utworzyc prosta hieracrchie klas. KLase bazowa punkt oraz klase pochodna wierzcholek. Punkt przechowuje wspolrzedne
	 * punktu typu double w dwuelementowej tab dyn. Wierzzcholek oprocz wspolrzednych, przechowuje tez informacje
	 * o kolorze swych obiektow. Obie klasy nalezy zapewnic indeksowany dostep do wartosci wspolrzednych
	 * oraz polimorficzna metode pozwalajaca uzyskac info dotyczaca kompletu przechowywania danych.
	 *
	 * DLa obu klas nalezy dostarcyc mechanizm pozwalajacy na wykonywanie mnozenia liczby calkowitej przez
	 * obiekt danej klasy [iloczyn liczby i wektora].
	 */



#include <iostream>

using namespace std;


class wierzcholek;

class punkt{
protected:
	unsigned int ile;
	double *wsk;
public:
    punkt():ile(0), wsk(new double(0)){}
    punkt(const double a1,const double a2):ile(2), wsk(new double[2])
    {
		wsk[0] = a1;
		wsk[1] = a2;
	}
	//konstruktor kopiujacy
	punkt(const punkt & a)
	{
        ile=a.ile;
		delete wsk;
		wsk = new double [ile];
		for(unsigned int i = 0; i<ile; i++)
		{
			wsk[i] = a.wsk[i];
		}
	}


    double & operator[](unsigned i)const{

        return wsk[i];

        }
	//operator przypisania
	const punkt & operator = (const punkt& a)
	{
		if(this!=&a)
		{
			ile = a.ile;
			delete wsk;
			wsk = new double[ile];
			for(unsigned int i = 0; i<ile; i++)
			{
				wsk[i] = a.wsk[i];
			}

		}
		return *this;
	}


	virtual ostream & pokaz (ostream &) const;

	friend ostream& operator<<(ostream & ,const punkt &);
	friend const punkt & operator*(double,const punkt&);

	~punkt(){delete [] wsk;}

};
ostream & punkt::pokaz (ostream & out) const
{
	out << "Punkt: " << " X: " << wsk[0] << " Y: " << wsk[1] << endl;
	return out;
}
ostream& operator<<(ostream& out, const punkt &p)
{
	return p.pokaz(out);
}
const punkt & operator*(double mn, const punkt&pp)
{

	return * new punkt(mn*pp[0], mn*pp[1]);

}


class wierzcholek:public punkt
{

	private:
		string kolor;
	public:
		wierzcholek():punkt(0,0),kolor("brak"){}
		wierzcholek(const double & a1, const double &a2, const string& a3):punkt(a1,a2),kolor(a3){}
		const double pokaz_x() const {return wsk[0];}
		const double pokaz_y() const {return wsk[1];}
	ostream & pokaz (ostream &) const;

	double& operator[](unsigned)const;
	friend const wierzcholek & operator*(double,const wierzcholek&);
//	friend ostream & operator << (ostream &, const wierzcholek & );
};

//ostream & operator << (ostream & out, const wierzcholek & a)
//{
//	return a.pokaz(out);
//}
ostream & wierzcholek::pokaz(ostream & out) const
{
	out << "Wierzcholek: " << kolor << " X: " << wsk[1] << " Y: " << wsk[2] << endl;
	return out;
}


const wierzcholek & operator*(double mn,const wierzcholek&pp){

	return * new wierzcholek(mn*pp[0], mn*pp[1],pp.kolor);

}



double& wierzcholek :: operator[](unsigned i)const{
    return wsk[i];

}



int main() {


   const punkt p1(1,1);
	const wierzcholek w1(2,2,"red");
	const punkt p2;
	const wierzcholek w2;
	cout << p2 << w2;//zainicjowane zerami

	cout << p1[0] << " " << p1[1] << endl;
    cout << w1[0] << " " << w1[1] << endl;

	cout << "********** PART 1 ************" << endl;
	punkt tp[2];
	wierzcholek tw[2];
	for (int i=0; i<2; i++){
		tp[i] = (i+2)*p1;//p1 = 1 1
		tw[i] = (i+2)*w1;//w1(2,2)
	}
	// dwa obiegi petli:
	/*
	 tp (2, 3) tw (4, 6)

	*/
	cout << "********** PART 2 ************" << endl;

	punkt *ta[4];
	ta[0] = &tp[0];//=2
	ta[1] = &tw[0];//=4
	ta[2] = &tp[1];//=3
	ta[3] = &tw[1];//=6

	for (int i =0; i<4; i++)
		cout << *ta[i]<<endl;
	cout << "********** FINITO ************" << endl;
    return 0;
    }
