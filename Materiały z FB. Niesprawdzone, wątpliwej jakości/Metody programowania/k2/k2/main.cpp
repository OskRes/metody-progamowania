#include <iostream>

using namespace std;

class figura
{
protected:
    string *kolor;
public:
    figura(): kolor(new string("brak")){}
    figura(const string &a1):kolor(new string(a1)){}
    figura(const figura &r):kolor(new string(*r.kolor)){}
    figura &operator=(const figura &r)
    {
        if(this!=&r)
        {
            *kolor=*r.kolor;
        }
        return *this;
    }

    virtual double pole()=0;

    virtual ostream &wypisz(ostream &f)
    {
        f << *kolor << endl;
        return f;
    }

    friend ostream &operator <<(ostream &out, figura &f);

    virtual ~figura()
    {
        delete kolor;
        cout << "destruktor figura" << endl;
    }

};
ostream &operator <<(ostream &out, figura &f)
{
        return f.wypisz(out);
}


class kolo: public figura
{
    double promien;
public:
    kolo():promien(0){}
    kolo(const string &b1, const double &b2):figura(b1), promien(b2){}
    kolo(const kolo &r):figura(*r.kolor), promien(r.promien){}
    kolo &operator=(const kolo &r)
    {
        if(this!=&r)
        {
            *kolor=*r.kolor;
            promien=r.promien;
        }
        return *this;
    }

    double pole()
    {
        return (3.14*promien*promien);
    }

    virtual ostream &wypisz(ostream &k)
    {
        k << *kolor << endl;
        k << promien << endl;
        return k;
    }

    friend ostream &operator <<(ostream &out, kolo &k);

    ~kolo(){cout << "destruktor kolo" << endl;}
};
ostream &operator <<(ostream &out, kolo &k)
{
        return k.wypisz(out);
}


class prostokat: public figura
{
    double boka;
    double bokb;
public:
    prostokat():boka(0), bokb(0){}
    prostokat(const string &c1, const double &c2, const double &c3): figura(c1), boka(c2), bokb(c3){}
    prostokat(const prostokat &r):figura(*r.kolor), boka(r.boka), bokb(r.bokb){}
    prostokat &operator=(const prostokat &r)
    {
        if(this!=&r)
        {
            *kolor=*r.kolor;
            boka=r.boka;
            bokb=r.bokb;
        }
        return *this;
    }

    double pole()
    {
        return boka*bokb;
    }

    virtual ostream &wypisz(ostream &p)
    {
        p << *kolor << endl;
        p << boka << endl;
        p << bokb << endl;
        return p;
    }

    friend ostream &operator <<(ostream &out, prostokat &p);

    ~prostokat(){cout << "destruktor prostokat" << endl;}

};

ostream &operator <<(ostream &out, prostokat &p)
{
    return p.wypisz(out);
}

int main()
{

    figura *tab[5];
    prostokat p1("yellow", 6, 9);
    kolo k1 ("red", 1);
    kolo k2 ("pink", 3);
    tab[0] = new kolo(k1);
    tab[1] = new prostokat("green", 2, 4);
    tab[2] = new kolo("blue", 2);
    tab[3] = new prostokat;
    tab[4] = new kolo;
    *tab[3]= p1;
    *tab[4] = k2;
    for (int i = 0; i<5; ++i)
        cout <<*tab[i] << tab[i]->pole();

    return 0;
}
