#include <iostream> //Jakub Jędrecki
#include <fstream>

using namespace std;

double **utworz( unsigned int n, unsigned int m)
{
	double **A = new double *[n];
	for(unsigned int i=0; i<n; i++)
	{
		A[i] = new double [m];
	}
	
	for(unsigned int i=0; i<n; i++)
	{
		for(unsigned int j=0; j<m; j++)
			A[i][j]=0;
	}
	
	return A;
}

bool czytaj (ifstream &fin, double **&A, unsigned int &n, unsigned int &m, double **&B, unsigned int &p, unsigned int &q)
{
	if(!fin)
	{
		cerr<<" problem ze strumieniem"<<endl;
		return false;
	}
	fin>>m>>n;
	A=utworz (n,m);
	for(unsigned int i=0; i<n; i++)
	{
		for(unsigned int j=0; j<m; j++)
			fin>>A[i][j];
	}
	fin>>p>>q;
	B=utworz (p,q);
	for(unsigned int i=0; i<p; i++)
	{
		for(unsigned int j=0; j<q; j++)
			fin>>B[i][j];
	}
	
	return true;
}

bool suma( double **A, const unsigned int n, const unsigned int m, double **B, const unsigned int p, const unsigned int q, double **&C)
{
	C = utworz(n,m);
	if( n == p && m == q)
	{
		for(unsigned int i=0; i<n; i++)
		{
			for(unsigned int j=0; j<m; j++)
				C[i][j] = A[i][j] + B[i][j];
		}
		return true;
	}
	return false;
}

void usun( double **X, const unsigned int n)
{
	for(unsigned int i=0; i<n; i++)
		delete []X[i];
	delete []X;
}

void zapisz(ostream &out, const double *const *C, const unsigned int m, const unsigned int n)
{
	for(unsigned int i=0; i<n; i++)
	{
		for(unsigned int j=0; j<m; j++)
			out<<C[i][j]<<" ";
		out<<endl;
	}
	out<<endl;
}

int main(int argc, char **argv)
{
	ifstream fin;
	
	fin.open(argv[1]);
	
	ofstream fout;
	//...
	double** A, ** B, ** C;
	unsigned int n, m, p, q;
	
	if (czytaj(fin, A, m, n, B, p, q)) {
	if(!suma(A, m, n, B, p, q, C))
	cerr << "macierze maja nieprawidlowe wymiary" << endl;
	else{
	fout << "suma macierzy" << endl;
	zapisz(cout, C, m, n);
	usun(C, m);
	}
/*	if(!roznica(A, m, n, B, p, q, C))
	cerr << "macierze maja nieprawidlowe wymiary" << endl;
	else{
	fout << "roznica macierzy" << endl;
	zapisz(fout, C, m, n);
	usun(C, m);
	}
	if(!iloczyn(A, m, n, B, p, q, C))
	cerr << "macierze maja nieprawidlowe wymiary" << endl;
	else{
	fout << "iloczyn macierzy" << endl;
	zapisz(fout, C, m, q);
	usun(C, m);
	}
	fout << "macierz przed transponowaniem" << endl;
	zapisz(fout, B, p, q);
	fout << "macierz transponowana" << endl;
	transpose(B, p, q, C);
	zapisz(fout, C, q, p);
	usun(C, q);
	usun(B,p);
	usun(A,m); */
	}
	//...
}

