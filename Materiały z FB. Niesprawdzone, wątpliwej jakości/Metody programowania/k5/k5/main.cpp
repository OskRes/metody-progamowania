#include <iostream>
#include <string>

using namespace std;

class figura
{
public:
    virtual const double &obwod()const=0;
    virtual const double &pole()const=0;
    virtual ~figura(){cout << "destruktor figura" << endl;}
};

class punkt:public figura
{
    double x;
    double y;
    double *roz;
public:
    punkt():x(0.0), y(0.0){}
    punkt(const double &a1, const double &a2):x(a1), y(a2), roz(new double(a2-a1)){}
    punkt(const punkt &r):x(r.x), y(r.y){}
    punkt &operator=(const punkt &r)
    {
        if(this!=&r)
        {
            x=r.x;
            y=r.y;
        }
        return *this;
    }

    virtual ostream &pokaz(ostream &p)
    {
        p << "Wspolrzedne punktu: " << x << " " << y << endl;
        return p;
    }

    friend ostream &operator<<(ostream &out, punkt &p);
};
ostream &operator<<(ostream &out, punkt &p)
{
    return p.pokaz(out);
}

int main()
{

//    punkt p[] = { punkt(0.0, 0.0), punkt(4.0, 0.0), punkt(4.0, 4.0), punkt(0.0, 4.0)};
    /*
    kwadrat k1(p, p+4);
    cout << "Pole kwadratu k1: " << k1.pole() << endl;
    trojkat t1(p, p+3, 4); // 4 - wysokosc trojkata
    cout << "Pole trojkata t1: " << t1.pole() << endl;
    cout << "Obwod trojkata t1: " << t1.obwod() << endl;
    t1[1] = punkt(5.0,0.0);
    cout << "Pole trojkata t1: " << t1.pole() << endl;
    cout << t1[0] << t1[1].x() << ", " << t1[1].y() << �\n� << t1[2] << endl;
    kwadrat k2(k1); t1[1] = k2[1];
    cout << "Pole trojkata t1: " << t1.pole() << endl;
    cout << k2 << endl
    return 0;
    */
}
