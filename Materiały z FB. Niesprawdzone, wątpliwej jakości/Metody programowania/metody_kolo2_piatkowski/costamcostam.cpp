#include <iostream>
#include <string>

using namespace std;

class komunikator
{
    protected:
    string *naz;
public:
    komunikator():naz(new string("Brak danych")){}
    komunikator(const string &a1):naz(new string(a1)){}
    komunikator(const komunikator &o):naz(new string(*o.naz)){}
    komunikator &operator=(const komunikator &o)
    {
        if(this!=&o)
        {
          *naz=*o.naz;
        }
        return *this;
    }

    virtual ~komunikator(){delete naz;}
    friend ostream &operator<<(ostream &out, const komunikator &kom);
    virtual void wypisz(ostream &out);
};
ostream &operator<<(ostream &out, const komunikator &kom)
{
    kom.wypisz(out);
    return out;
}

class k1: public komunikator
{
public:
    k1():komunikator(){}
    k1(const string &n):komunikator(n){}
    k1(const k1 &x):komunikator(x){}
    k1 &operator=(const k1 &x)
    {
        if(this!=&x)
        {
            komunikator::operator=(x);
        }
        return *this;
    }

    void wyp(ostream &out){out << *naz << endl;}
    ~k1(){}
};

class k2:public komunikator
{
private:
    int zm;
public:
    k2():komunikator(), zm(0){}
    k2(const string &a1, int a2):komunikator(a1), zm(a2){}
    k2(const k2 &o):komunikator(o), zm(o.zm){}
    k2 &operator=(const k2 &o)
    {
        if(this!=&o)
        {
            komunikator::operator=(o);
            zm=o.zm;
        }
        return *this;
    }

    void fun(ostream &out)
    {
        out << *naz << " " << zm << endl;
    }

    ~k2(){}
};

int main()
{
  const k1 koniec("Koniec komunikatow") ;

  komunikator *linia[5];
  linia[0]= new k1("Temperatury powietrza");
  linia[1]= new k2("Czestochowa", -5);
  linia[2]= new k1("Opady sniegu");
  linia[3]= new k2("Katowice", 10);
  linia[4]= new k1(koniec);

  for (int i=0;i<5;i++)
    cout << *linia[i];

    cout << endl;
    cout << "********* 3 **********" << endl;
    cout << endl;

}
