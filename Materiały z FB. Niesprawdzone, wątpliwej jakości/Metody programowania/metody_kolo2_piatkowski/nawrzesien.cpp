#include <iostream>
#include <string>

using namespace std;

class osoba
{
protected:
    string *T;
public:
    osoba():T(new string[2]){T[0]="Brak"; T[1]="Brak";}
    osoba(const string &a1, const string &a2):T(new string[2]){T[0]=a1; T[1]=a2;}
    osoba(const osoba &o):T(new string(*o.T)){T[0]=o.T[0]; T[1]=o.T[1];}
    osoba &operator=(const osoba &o)
    {
        if(this!=&o)
        {
            delete[]T;
            T=new string[2];
            T[0]=o.T[0];
            T[1]=o.T[1];
        }
        return *this;
    }
    ~osoba(){delete [] T;}

    string &operator[](int i)
    {
        return T[i];
    }
    const string &operator[](int i)const
    {
        return T[i];
    }

    friend ostream &operator<<(ostream &out, const osoba &o)
    {
        return o.pokaz(out);
    }

    virtual ostream &pokaz(ostream &out)const
    {
        out << T[0] << " " << T[1] << endl;
    }

    friend osoba operator+(const string &d, const osoba &o)
    {
        o.T[1]=d+" "+o.T[1];
        return o;
    }

};

class student:public osoba
{
private:
    int nr;
public:
    student():osoba(), nr(0){};
    student(const string &b1, const string &b2, int b3):osoba(b1,b2), nr(b3){}
    student(const student &s):osoba(s), nr(s.nr){}
    student &operator=(const student &s)
    {
        if(this!=&s)
        {
            osoba::operator=(s);
            nr=s.nr;
        }
        return *this;
    }

    ostream &pokaz(ostream &out)const
    {
        out << T[0] << " " << T[1] << " " << nr << endl;
    }

    friend student operator+(const string &d, const student &s)
    {
        s.T[1]=d+" "+s.T[1];
        return s;
    }

    ~student(){}
};

int main()
{
    //..
    const osoba os1;
    const student st1;
    const osoba os2("Ela", "Kowalska");
    const student st2("Ula", "Nowakowska", 1542);

    const osoba *wsk = &os2;
    cout << *wsk;
    wsk = &st2;
    cout << *wsk;

  //  try{
        cout << os1[0] << " " << os1[1] << endl;
        cout << st1[0] << " " << st1[1] << endl;
   //     cout << st2[2];
   // } catch

    osoba *tab[4];
    tab[0] = new osoba(os2);
    tab[1] = new osoba("Nowak - " + os2);
    tab[2] = new student(st2);
    tab[3] = new student("Lis - " + st2);

    for(int i =0; i < 4; i++){
        cout << *tab[i];
        delete tab[i];
    }

    return 0;
}
