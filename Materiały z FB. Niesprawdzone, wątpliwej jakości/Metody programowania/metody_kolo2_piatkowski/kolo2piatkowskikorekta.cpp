#include <iostream>
using namespace std;

class komunikator {
protected:
    string *naz;

public:
    komunikator (): naz (new string ("Brak danych")) {liczba_obiektow +=1;}
    komunikator (const string& a1): naz(new string (a1)) {liczba_obiektow +=1;}
    komunikator (const komunikator & k): naz (new string (*k.naz)){liczba_obiektow +=1;}
    komunikator& operator= (const komunikator & k){
      if( this!=&k){
        *naz=*k.naz;

      }
      return *this;
    }
    static unsigned liczba_obiektow;
    komunikator& operator+= (const string &a)
    {
        *naz += a;
        return *this;
    }
    virtual ~komunikator () {delete naz; liczba_obiektow -=1;}

    friend ostream& operator<< (ostream& out, const komunikator& kom);

    virtual void fun (ostream & out) const =0;
};
unsigned komunikator::liczba_obiektow = 0;

ostream& operator<< (ostream& out, const komunikator& kom){
kom.fun (out);
return out;
}

class k1 :public komunikator{

public:
    k1 ():komunikator(){}
    k1 (const string& n):komunikator (n) {}
    k1 (const k1& n): komunikator(n) {}
    k1& operator= (const k1& n){
      if( this!=&n){
        komunikator::operator=(n);
      }
      return *this;

    }
    void fun (ostream & out) const
    { out << *naz << endl;}

    ~k1(){}
};


class k2 :public komunikator{

int zm;

public:
    bool temp;    // ta skladowa temp bedzie sluzyla do rozpoznawania czy obiekt tworzony jest konstruktorem domyslnym, bo jesli tak to program nie powinien wyswietlac wartosci temperatury bo jest nieznana.
    k2(): zm(){temp = true;}
    k2(const string& a1, int a2):komunikator(a1), zm(a2){}
    k2(const k2& k): komunikator(k), zm(k.zm){}
    k2& operator= (const k2& n){
      if( this!=&n){
        komunikator::operator=(n);
        zm=n.zm;
      }
      return *this;
    }

    void fun(ostream & out) const
    {
        out << *naz << "  ";
       if (temp !=true){out << zm;}
        out << endl;
    }

    ~k2(){}
};
int main ()
{
  const k1 koniec("Koniec komunikatow") ;

  komunikator *linia[5];
  linia[0]= new k1("Temperatury powietrza");
  linia[1]= new k2("Czestochowa", -5);
  linia[2]= new k1("Opady sniegu");
  linia[3]= new k2("Katowice", 10);
  linia[4]= new k1(koniec);

  for (int i=0;i<5;i++)
    cout << *linia[i];

    cout << endl;
    cout << "********* 3 **********" << endl;
    cout << endl;

    *linia[0] += " [oC]:";
    *linia[2] += " [cm]:";

    for (int i=0; i<5; ++i){
        cout << *linia[i];
        delete linia[i];
    }
    cout << endl;
    cout << "********* 4 **********" << endl;
    cout << endl;

    cout << "Liczba dzialajacych komunikatorow: "
         << komunikator::liczba_obiektow << endl;


     linia[0] = new k1("Temperatury powietrza:");
     linia[1] = new k2;
     linia[2] = new k1("Opady sniegu (cm):");
     linia[3] = new k2;
     linia[4] = new k1(koniec);
     cout << endl;
     cout << "Liczba dzialajacych komunikatorow: "
          << komunikator::liczba_obiektow << endl;

    cout << endl;
    for (int i=0; i<5; ++i)
        try{
        cout << *linia[i];
        }
        catch (const string &e) {cout << e;}

        cout << endl;
        cout << "********* 5 ***********" << endl;

return 0;
}
