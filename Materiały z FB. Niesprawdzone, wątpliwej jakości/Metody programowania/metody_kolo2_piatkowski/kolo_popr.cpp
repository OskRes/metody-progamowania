#include <iostream>
using namespace std;

class osoba
{
    protected:
        string *T;

    public:
        osoba():T(new string[2]){T[0] = "brak"; T[1] = "brak";}
        osoba(const string &a, const string &b):T(new string[2])
        {
            T[0] = a;
            T[1] = b;
        }
        osoba(const osoba &o):T(new string[2])
        {
            T[0] = o.T[0];
            T[1] = o.T[1];
        }
        ~osoba(){delete [] T;
        }
        virtual ostream &pokaz(ostream &wy) const
        {
            return wy << "(" << T[0] << " " << T[1] << ")" << endl;
        }
        friend ostream &operator << (ostream &wy, const osoba &o)
        {
            return o.pokaz(wy);
        }
        string &operator [] (int i)
        {
            return T[i];
        }
        const string &operator [] (int i)const
        {
            return T[i];
        }
/*
        osoba &operator + (const string &t)
        {
            T[1] = t+""+T[1];
            return *this;
        }
        const osoba &operator + (const string &t)const
        {
            T[1] = t+""+T[1];
            return *this;
        }
*/
        friend osoba operator + (const string &t, const osoba &o)
        {
            o.T[1] = t+""+o.T[1];
            return o;
        }


};

class student : public osoba
{
    private:
        int NR;

    public:
        student():osoba(), NR(0){}
        student(const string &a, const string &b, int n):osoba(a,b), NR(n){}

        ostream &pokaz(ostream &wy) const
        {
            return wy << "(" << T[0] << " " << T[1] << " " << NR << ")" << endl;
        }
        friend student operator + (const string &t, const student &s)
        {
            s.T[1] = t+""+s.T[1];
            return s;
        }
};



int main()
{
    //..
    const osoba os1;
    const student st1;
    const osoba os2("Ela", "Kowalska");
    const student st2("Ula", "Nowakowska", 1542);

    const osoba *wsk = &os2;
    cout << *wsk;
    wsk = &st2;
    cout << *wsk;

  //  try{
        cout << os1[0] << " " << os1[1] << endl;
        cout << st1[0] << " " << st1[1] << endl;
   //     cout << st2[2];
   // } catch

    osoba *tab[4];
    tab[0] = new osoba(os2);
    tab[1] = new osoba("Nowak - " + os2);
    tab[2] = new student(st2);
    tab[3] = new student("Lis - " + st2);

    for(int i =0; i < 4; ++i){
        cout << *tab[i];
        delete tab[i];
    }

    return 0;
}
