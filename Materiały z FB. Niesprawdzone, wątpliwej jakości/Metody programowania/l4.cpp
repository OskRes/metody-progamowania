#include <iostream>

using namespace std;


class adres{
  string miasto;
  string kod;
  string ulica;
  int nr;

public:
  adres():miasto("brak"), kod("brak"), ulica("brak"), nr(0){}
  adres(const string &a1, const string &a2, const string &a3, int a4):
      miasto(a1), kod(a2), ulica(a3), nr(a4){}
friend
  ostream& operator <<(ostream&, const adres&);

};

ostream& operator <<(ostream& out, const adres& r){
  return out << r.miasto << " " << r.kod << " " << r.ulica << " "
     << r.nr << " ";
}



class osoba{
  string im;
  string naz;
  int w;
  adres *wsk;

  public:
    osoba():im("brak"), naz("brak"), w(0), wsk(new adres){}
    osoba(const string &a1, const string &a2, int a3, const adres &a4):
      im(a1), naz(a2), w(a3), wsk(new adres(a4)){}

  ~osoba(){delete wsk;}

  osoba(const osoba&r):im(r.im), naz(r.naz), w(r.w), wsk(new adres(*r.wsk)){}

  osoba& operator=(const osoba& r){
    if(this!=&r)
    {
      im=r.im;
      naz=r.naz;
      w=r.w;
      *wsk=*r.wsk;
    }

    return *this;
  }

  friend
    ostream& operator <<(ostream&, const osoba&);

};

ostream& operator <<(ostream& out, const osoba& r){
  return out << r.im << " " << r.naz << " " << r.w << " "
     << *r.wsk << " ";
}



int main()
{
  adres* wsk = new adres("Czestochowa", "42-200", "Dabrowskiego", 73);
  cout << *wsk << '\n';

  adres a1(*wsk);
  delete wsk;

  const adres* wsk1 = new adres("Warszawa", "00-950", "Mysliwiecka", 357);
  cout << a1 << '\n';
  cout << *wsk1 << '\n';
  adres a2;
  cout << a2 << '\n';
  a2 = a1;
  cout << a2 << '\n';

  osoba o("Jan", "Kos", 25, *wsk1);

  delete wsk1;

  cout << o << '\n';
  osoba o1(o);
  cout << o1 << '\n';
  osoba o2;
  cout << o2 << '\n';
  o2 = o1;
  cout << o2 << '\n';



  return 0;
}
