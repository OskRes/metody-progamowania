#include <iostream>
#include <string>

using namespace std;

class car
{
    string nazwisko;
    double *kilom;
public:
    car():nazwisko("brak"), kilom(0){}
    car(string a1, double a2):nazwisko(a1), kilom(new double(a2)){}
    car(const car &r):nazwisko(r.nazwisko), kilom(new double(*r.kilom)){}
    car &operator=(car &r)
    {
      if(this!=&r)
      {
        r.nazwisko=nazwisko;
        *r.kilom=*kilom;
      }
      return *this;
    }

    const string &kierowca()const{return nazwisko;}
    const double &kilometry()const{return *kilom;}

    ostream &pokaz(const car &x)
    {
        x << nazwisko << endl;
        x << *kilom << endl;
        return x;
    }

    friend ostream &operator<<(ostream &out, const car &x);

    ~car(){delete kilom;}

};

int main()
{

    car *wsk = new car("Nowak", 5555.5);
    car ob2(*wsk), t[3];
    const car ob3("Kot", 10000.0);
    delete wsk;
    wsk = new car("Kowalski", 22000.2);

    t[0]=ob2;
    t[1].kierowca()=ob3.kierowca();
    t[1].kilometry()=ob3.kilometry();
/*
    t[2].kierowca()=wsk->kierowca();
    t[2].kilometry()=wsk->kilometry()+1000;

    for(int i=0; i<3; i++)
    {
        cout << t[i];
    }

    cout << "********* 3 ********" << endl;

    for(int i=0; i<3; i++)
    {
        t[i]+=1000;
        cout << t[i];
    }

    cout << "********* 4 *******" << endl;
*/
    return 0;
}
