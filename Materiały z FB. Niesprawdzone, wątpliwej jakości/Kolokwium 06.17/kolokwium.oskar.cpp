#include <iostream>

using namespace std;

class K1
{
  string *p1; //wskaźnik na stringa

public:
  //zdefiniowany konstruktor domyślny, musi coś być podstawione pod "p1" bo jeszcze ktoś użyje.
  //NIE MOŻNA używać nieustawionych wskażników dlatego najlepiej ustawić je choćby na "0" lub "NULL" albo coś domyślnego.
  // mogłoby być:
  // K1() : p1(new string()) {}
  // K1() : p1(0) {}
  // K1() : p1(NULL) {}
  K1() : p1(new string("brak")) {}
  //Zwykły konstruktor
  K1(const string &s) : p1(new string(s)) {}
  //Konstruktor kopiujący ze wskaźnika
  K1(const K1 &old) : p1(new string(*old.p1)) {}
  //destruktor, który musi być jeśli mamy jakiś wskaźnik. Tu jest to "p1"
  ~K1()
  {
    delete p1;
  }
  //pokazuje wartość "p1" ale TYLKO DO ODCZYTU
  //pierwszy const oznacza, że zwrócony string jest stały,
  //drugi const oznacza, że w funkcji nie ma niczego co zmieniłoby jakąś wartość tej klasy
  //np. jakby był jakiś x++ czy coś
  const string &pokaz() const { return *p1; };
  //pokazuje wartość "p1" ale DO ODCZYTU i DO ZAPISU
  string &pokaz() { return *p1; };
}; //koniec klasy

class K2
{
  //tablica 2 elementowa obiektów K1
  K1 dane[2];

public:
  //konstruktor domyślny
  K2() : dane() {}
  //tu jest prosta rzecz:
  // : dane({*(new K1(s1)), *(new K1(s2))}) to kolejno od środka
  // 1. s1 to string
  // 2. new K1(s1) tworzy obiekt klasy K1 i zwraca WSKAŹNIK
  // 3. *(new K1(s1)) to wyłuskanie wartości - mamy teraz OBIEKT klasy K1
  // 4. {} nawiasy klamrowe to INICJACJA TABLICY
  // możnaby to zrobić w kilku linijkach, np.:
  // K2(const string s1, const string s2)
  // {
  // K1 *a = new K1(s1); //pkt. 1 i 2
  // K1 *b = new K1(s2); //pkt. 1 i 2
  // dane[0] = *a; //pkt. 3 i 4
  // dane[1] = *b; //pkt. 3 i 4
  // };

  K2(const string s1, const string s2) : dane({*(new K1(s1)), *(new K1(s2))}) {}
  //Konstruktor kopiujący. To samo co wyżej tylko zamiast string s1 mam old.dane[0] a zamiast string s2 mam old.dane[1]
  K2(const K2 &old) : dane({*(new K1(old.dane[0])), *(new K1(old.dane[1]))}) {}
  //deklaracja funkcji operator<< - NIE MA nazw zwiennych
  friend ostream &operator<<(ostream &, const K2);
  //definicja funkcji oprerator= - MA nazwy zmiennych
  void operator=(const K2 k)
  {
    dane[0] = *(new K1(k.dane[0].pokaz()));
    dane[1] = *(new K1(k.dane[1].pokaz()));
  };
  //definicje funkcji operator[] - zwraca wartość z tablicy jako stałą. patrz wiersz 26 - 29
  const string &operator[](const int i) const
  {
    if (i == 0 or i == 1)
      return dane[i].pokaz();
    return (*new string("zły zakres"));
  };
  //definicje funkcji operator[] - zwraca wartość z tablicy jako zmienną. patrz wiersz 31
  string &operator[](const int i)
  {
    if (i == 0 or i == 1)
      return dane[i].pokaz();
    return (*new string("zły zakres"));
  };
}; //koniec klasy

//deklaracja funkcji operator<< - MA nazwy zwiennych
ostream &operator<<(ostream &o, K2 k)
{
  return o << k.dane[0].pokaz() << " " << k.dane[1].pokaz() << endl;
}

//treść z fotografii kolokwium.jpg
//kompilowanie : g++ -Wall -pedantic kolokwium.cpp -o kolokwium.exe
int main()
{
  K2 o1, o2;
  const K2 *wsk1 = new K2("Ala", "Ola");
  const K2 o3(*wsk1);

  cout << "o2 : " << o2 << endl;
  cout << "o3 : " << o3 << endl;
  cout << "********" << endl;
  delete wsk1;
  wsk1 = 0;

  wsk1 = new K2(o3);
  o2 = *wsk1;
  cout << "o1 : " << o1 << endl;
  cout << "o2 : " << o2 << endl;
  cout << "*****" << endl;
  delete wsk1;
  wsk1 = 0;

  o1 = K2("Ewa", "Iza");
  cout << o1[0] << ", " << o1[1] << endl;
  o1[1] = "Jan";
  cout << o1[0] << ", " << o1[1] << endl;

  return 0;
}