#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class K2;

class K1
{
  string *p1;
public:
  K1():p1(new string("brak")){};
  K1(string napis):p1(new string(napis)){};
  string& getstr(){return *p1;}
  const string& getstr()const {return *p1;}

  K1& operator=(const char* obj)
  {
      {
          p1 = new string(obj);
      }
      return *this;
  }

  friend ostream& operator<<(ostream& out,const K1& obj);


  //~K1(){delete p1;}
};

ostream& operator<<(ostream& out,const K1& obj)
{
	out << obj.getstr() << " ";
	return out;
}

class K2
{
  K1 dane[2];
public:
  K2()
	{
	  dane[0] = K1("brak");
	  dane[1] = K1("brak");
	}
  K2(const string imie1, const string imie2)
  {
	  dane[0] = K1(imie1);
	  dane[1] = K1(imie2);
  }

  K2(const K2& obj)
  {
	  dane[0] = obj.dane[0];
	  dane[1] = obj.dane[1];

  }
  friend ostream& operator<<(ostream& out,const K2& obj);
  K2& operator=(const K2& obj)
  {
      if(this != &obj)
      {
          dane[0]=obj.dane[0];
          dane[1]=obj.dane[1];
      }
      return *this;
  }

  K1& operator[](int i)
  {
	  return dane[i];
  }


};

ostream& operator<<(ostream& out, const K2& obj)
{
	out << obj.dane[0].getstr() << " " << obj.dane[1].getstr() << endl;
	return out;
}




int main()
{
	K2 o1, o2;
	const K2* wsk1 = new K2("ALA", "OLA");
	const K2 o3(*wsk1);

	cout <<"o2 :"<< o2 <<endl;
	cout <<"o3 :"<< o3 <<endl;
	cout<<"********"<<endl;
	delete wsk1;
	wsk1=0;

	wsk1 = new K2(o3);
	o2=*wsk1;
	wsk1 = 0;

	cout<<"o1 : "<< o1 << endl;
	cout<<"o2 : "<< o2 << endl;

	cout<<"*****"<<endl;
	delete wsk1;
	wsk1 = 0;
	o1 = K2("Ewa", "Iza");
	cout << o1[0]<<", "<< o1[1] << endl;
	o1[1] = "Jan";
	cout << o1[0] <<", " << o1[1] << endl;

return 0;
}