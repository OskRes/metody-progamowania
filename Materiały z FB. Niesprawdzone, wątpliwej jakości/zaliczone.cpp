#include <iostream>

using namespace std;

class osoba {
  string *im, naz;
  int *wi;

public:
  osoba() : im(new string("")), naz(""), wi(new int()) {}

  osoba(const string &a, const string &b, int c) {
    im = new string(a);
    naz = b;
    wi = new int(c);
  }

//  osoba(const osoba &a) : im(new string(*a.im)), naz(a.naz), wi(new int(*a.wi)) {}

  osoba(const osoba &k) {
    im = new string(*k.im);
    naz = k.naz;
    wi = new int(*k.wi);
  }

  /// kopiuj1cy
  osoba &operator=(const osoba &r) /// przeci1?enie przypisania
  {
    if (this != &r) {
      im = new string(*r.im);
      naz = r.naz;
      wi = new int(*r.wi);
    }
    return *this;
  }

  friend ostream &operator<<(ostream &out, const osoba &r) {
    return out << *r.im << " " << r.naz << " " << *r.wi;
  }

  osoba operator+(string a) {
    return osoba(*im + a, naz, *wi);
  }

  osoba operator+(int b) {
    return osoba(*im, naz, *wi + b);
  }

  ~osoba() {
    delete im;
    delete wi;
  }
};

int main() {
  osoba o1, o2;
  const osoba *wsk1 = new osoba("Ala", "Koala", 15);
  cout << endl;
  const osoba o3(*wsk1);
  cout << endl;
  delete wsk1;
  wsk1 = 0;
  const osoba *wsk2 = new osoba(o3);
  cout << endl;
  o2 = *wsk2;
  cout << o1 << endl << *wsk2;
  cout << endl;
  delete wsk2;
  wsk2 = 0;

  const osoba *wsk5 = new osoba("Jan ", "Wrobel ", 40);
  osoba o5(*wsk5);
  cout << o2;
  cout << endl;
  cout << o2 + 10;
  cout << endl;
}