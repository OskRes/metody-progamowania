#include <iostream>
#include <cmath>

using namespace std;

class point {
  double xyz[3];
public:
  point() : xyz{0, 0, 0} {}

  point(const double a, const double b = 0.0, const double c = 0.0) : xyz{a, b, c} {}

  point(const double (&a)[3]) : xyz{a[0], a[1], a[2]} {}

  const double distance(const point &p) const {
    return sqrt(pow(xyz[0] - p.xyz[0], 2) + pow(xyz[1] - p.xyz[1], 2) + pow(xyz[2] - p.xyz[2], 2));
  }

  friend ostream &operator<<(ostream &, const point &);

  friend istream &operator>>(istream &, point &);

  friend const point &operator+(const point &, const point &);

  friend const point &operator-(const point &, const point &);

  friend const point &operator*(const double &, const point &);

  friend const point &operator*(const point &, const double &);

  friend const bool operator<(const point &, const point &);

  friend const bool operator>(const point &, const point &);

  friend const bool operator==(const point &, const point &);

  const double operator[](const int &i) const {
    return xyz[i];
  };
};

ostream &operator<<(ostream &out, const point &a) {
  return out << "[x,y,z] = [" << a.xyz[0] << ", " << a.xyz[1] << ", " << a.xyz[2] << "]";
}

istream &operator>>(istream &in, point &a) {
  in >> a.xyz[0] >> a.xyz[1] >> a.xyz[2];
  return in;
}

const point &operator+(const point &p1, const point &p2) {
  return *(new point(p1.xyz[0] + p2.xyz[0], p1.xyz[1] + p2.xyz[1], p1.xyz[2] + p2.xyz[2]));
}

const point &operator-(const point &p1, const point &p2) {
  return *(new point(p1.xyz[0] - p2.xyz[0], p1.xyz[1] - p2.xyz[1], p1.xyz[2] - p2.xyz[2]));
}

const point &operator*(const double &multi, const point &p) {
  return *(new point(multi * p.xyz[0], multi * p.xyz[1], multi * p.xyz[2]));
}

const point &operator*(const point &p, const double &multi) {
  return *(new point(multi * p.xyz[0], multi * p.xyz[1], multi * p.xyz[2]));
}

const bool operator<(const point &p1, const point &p2) {
  return p1.distance(*new point) < p2.distance(*new point);
}

const bool operator>(const point &p1, const point &p2) {
  return p1.distance(*new point) > p2.distance(*new point);
}

const bool operator==(const point &p1, const point &p2) {
  return p1.distance(*new point()) == p2.distance(*new point);
}

int main() {
  double x[2][3] = {{1.0, 1.0, 1.0},
                    {1.0, 2.0, 3.0}};
  point p1(x[0]), p2(x[1]);
  const point p3(0.4, 0.2, 0.1);

  cout << p1 << ", " << p2 << '\n';
  cout << p3[0] << ' ' << p3[1] << ' ' << p3[2] << '\n';
  cout << p1.distance(point()) << ", " << p3.distance(p1) << '\n';

  cout << p1 + p2 << ", " << p1 - p3 << '\n';
  cout << 3.14 * p2 << ", " << p2 * 3.14 << '\n';
  cout << (p1 < p3) << ", " << (p1 == point(1.0, 1.0, 1.0)) << '\n';

  cin >> p1;
  cout << p1 << '\n';

  return 0;
}
