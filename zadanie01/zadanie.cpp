#include <iostream>
#include <fstream>
#include <sstream>

struct Student
{ //struktura to taka prymitywna klasa, może mieć metody (w tym konstruktory), ale traktuje się je jako nośnik danych (np. wiersz w bazie danych), a nie obiekt, który oprócz danych ma też możliwość ich obróbki. Dodałbym tu 3 konstruktory (domyślny i 2 które są napisane jako funkcje "studentCreate") i 4 metody (teraz to zewnęrzne "setImie", "setNazwisko", "setIndeks", "studentPrint"), ale zadanie mówi, że struktura ma tylko 3 pola.
  std::string imie;
  std::string nazwisko;
  unsigned int numer;
};

Student studentCreate(std::string &i, std::string &n, int nr) //funkcja, która tworzy nowy obiekt struktury Student na podstawie 3 zmiennych (imię, nazwisko, indeks). W praktyce to taki "konstruktor poza strukturą". Można to przenieść i przebudować na konstruktor.
{
  Student newStudent; //tworzę gościa i kolejno ustawiam mu wartości a na końcu zwracam
  newStudent.imie = i;
  newStudent.nazwisko = n;
  newStudent.numer = nr;
  return newStudent;
}
Student studentCreate(std::string &line) //to jest inna wersja "konstruktora poza strukturą". Zamiast 3 zmiennych przyjmuje 1 stringa który zawiera "<imię> <nazwisko> <nr indeksu>", np. "Kamil Stoch 125"
{
  Student newStudent;                    //tworzę gościa, prawdziwy konstruktor (choć tylko domyślny)
  std::stringstream oneLineStream(line); //tworzę strumień striga, który dostaje do konstruktora 1 stringa z 3 zmiennymi, np. wymieniony "Kamil Stoch 125"
  oneLineStream >> newStudent.imie;      // z "Kamil Stoch 125" jest pobierana pierwsza wartość (do spacji, tabulatora, entera, itp.). Tu "Kamil" i przesuwa "kursor" na miejsce za "Kamil"
  oneLineStream >> newStudent.nazwisko;  // z "Kamil Stoch 125" jest pobierana druga wartość ("Stoch"), bo jest za "kursorem". Kursor został na spacji za "Stoch"
  oneLineStream >> newStudent.numer;     // z "Kamil Stoch 125" jest pobierana 3 wartość, która wynosi 125.
  return newStudent;                     //no i mamy gotowy obiekt, który zwracamy
}

void setImie(Student &st, std::string *value) //funkcja ma 2 argumenty (<Student, któremu zmieniam imię> <wskaźnik na nową wartość imienia>) i podmienia. Mogłaby to być metoda w strukturze Student
{
  st.imie = *value;
}
void setNazwisko(Student &st, std::string *value) //jak setImie
{
  st.nazwisko = *value;
}
void setIndeks(Student &st, int *value) //jak setImie
{
  st.numer = *value;
}

std::ostream &operator<<(std::ostream &out, const Student &st)
{
  return out << "* Imię: " << st.imie << ", Nazwisko: " << st.nazwisko << ", Nr indeksu: " << st.numer << '\n';
}
void studentPrint(Student **array, int length)
{
  for (int i = 0; i < length; i++)
    // studentPrint((*array)[i]);
    std::cout << (*array)[i];
}

int saveToFile(Student **studentInput, std::string *file, int length) //zapis do pliku przyjmuje wskaźnik do tablicy zawierających obiekty Stuident, wskaźnik do nazwy pliku, ilość
{
  std::stringstream output("");    //podobnie jak w studentCreate(std::string &line), tworzę strumień stringa (to taki worek do obsługi/edycji stringów)
  output << length << '\n';        //wstawiam do strumienia ilość studentow
  for (int i = 0; i < length; i++) //w pętli pracuję nad każdym z elementów tablicy
  {
    output << (*studentInput)[i].imie << " " << (*studentInput)[i].nazwisko << " " << (*studentInput)[i].numer << '\n'; //wstawiam do strumienia kolejnych studentów
  }
  std::ofstream outputFile(*file); //tworzę drugi strumień, tym razem strumień plikowy (wyjście/zapis), na podstawie wskaźniaka z argumentów
  outputFile << output.str();      //wrzucam cały utworzony strumień stringowy do strumienia plikowego.
  outputFile.close();              //zamykam plik
  return 0;
}

int readFromFile(Student **studentInput, std::string *file) //odczytywanie studentów z pliku
{
  std::ifstream inputFile(*file); //tworzę strumień plikowy (wejście/odczyt)
  if (!inputFile.is_open())       //sprawdzam czy wyszło, taka wersja "wyrzucania wyjątków". Nie chciałem wprowadzać wyjątków skoro jeszcze ich nie było
  {
    std::cout << "Błąd otwarcia pliku" << '\n';
    return -1;
  }
  int rows = 0;             //tu będzie pierwszy wiersz odczytany z pliku
  if (!(inputFile >> rows)) //odczyt pierszego wiersza i wrzucenie go do rows. Jeśli się nie uda to mamy rzucenie "wyjątku dla ubogich"
  {
    std::cout << "Pierwszy wiersz nie jest liczbą" << '\n';
    return -2;
  }
  if (rows < 1) //jeśli była zła wartość, np. literowa, albo int by za mały to tu mamy kolejny biedawyjątek
  {
    std::cout << "Zbyt mała liczba wierszy" << '\n';
    return -3;
  }
  *studentInput = new Student[rows]; //skoro wsio zadziałało tworzymy: tablicę Studentów, stringa do którego będziemy wrzucać odczytany wiersz (kursor jest na początku DRUGIEGO!! wiersza)
  std::string line;
  getline(inputFile, line); //wczytanie całej linii i przekazanie do zmiennej line, ale tak naprawdę to przejście do nowego wiersza, bo "inputFile >> rows" odczytuje wartość i przerywa przy "białym znaku", np. spacji albo enterze
  for (int i = 0; i < rows; i++)
  {
    getline(inputFile, line);                 //odczyt całej linii, powinien tam być student o składni "<imię> <nazwisko> <indeks>", np. Adam Sienkiewicz 1410 i przekazanie go do zmiennej "line"
    (*studentInput)[i] = studentCreate(line); //tworzenie studenta na podstawie drugiego studentCreate(std::string &line) i dodanie wskażnika tego studenta do tablicy
  }
  inputFile.close(); //zamykamy plik
  return rows;       //zwracamy wiersze lub błąd (biedawyjątek) jeśli wartość jest < 0
}

int main()
{
  using namespace std;
  string sourcePath = "./Student.txt";                     //Nazwa pliku wejściowego
  string destinationPath = "./Wynik";                      //Nazwa pliku wyjściowego
  Student *studentsFromFile = NULL;                        //wskaźnik na tablicę studentów
  int rows = readFromFile(&studentsFromFile, &sourcePath); //tu przekazuję: wskaźnik na wskaźnik do tablicy studentów (bo mam taki nawyk), nazwę pliku z ktorego czytam. Jak jest na końcu readFromFile(...), funkcja prócz modyfikacji tablicy zwraca wartość równą liczbie wierszy lub "kod błędu"
  if (rows < 0)                                            //nie robimy jeśli zwrócony był kod błędu
  {
    std::cout << "Błąd nr " << rows << " w odczycie pliku" << '\n';
    return rows;
  }
  studentPrint(&studentsFromFile, rows);
  saveToFile(&studentsFromFile, &destinationPath, rows);
  string a = "Asia";
  cout << studentsFromFile[1];
  string b = "Szymańska";
  int c = 18751101;
  setImie(studentsFromFile[1], &a);
  setNazwisko(studentsFromFile[1], &b);
  setIndeks(studentsFromFile[1], &c);
  cout << studentsFromFile[1];
}
