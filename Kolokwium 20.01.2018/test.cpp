#include <iostream>
#include <fstream>

using namespace std;

class K1 {
  string * p1;
public:
  K1(string s1="brak 1", string s2="brak 2"){
    p1 = new string[2];
    p1[0] = s1;
    p1[1] = s2;
  }
  K1(const K1 &o){
    p1 = new string[2];
    p1[0] = o.p1[0];
    p1[1] = o.p1[1];
  }
  ~K1(){
    delete[] p1;
  }
  K1& operator=(const K1& o) {
    delete[] p1;
    p1 = new string[2];
    p1[0] = o.p1[0];
    p1[1] = o.p1[1];
    return *this;
  }
  const string& getstring(int i) const {
    return p1[i];
  }
  friend ostream& operator<<(ostream &, const K1&);
};
ostream& operator<<(ostream &o, const K1&k){
  return o << k.p1[0] << " " << k.p1[1] << " ";
}

class K2 {
  K1 w1;
  double w2;
public:
  K2():w1(),w2(-1){};
  K2(string s1, string s2, double d3):w1(s1,s2),w2(d3){};
  void operator=(K2 o) {
    w1=o.w1;
    w2=o.w2;
  }
  friend ostream& operator<<(ostream &, const K2&);
  K2& operator-(double d) {
    w2-=d;
    return *this;
  }
  void operator+=(double d){
    w2+=d;
  }
  const K1& getK1(){
    return w1;
  }
  friend K2& operator+(K2, string);
  friend K2& operator+(string, K2);
};
ostream& operator<<(ostream &o, const K2&k){
  return o << k.w1 << k.w2<<endl;
}

K2& operator+(K2 k, string s){
  return *(new K2(k.getK1().getstring(0),k.getK1().getstring(1)+s,k.w2));
}
K2& operator+(string s, K2 k){
  return *(new K2(k.getK1().getstring(0)+s,k.getK1().getstring(1),k.w2));
}


int main() {
  K2 ob1,ob2;
  const K2 * wsk1 = new K2("kawa", "z mlekiem", 4.50);
  const K2 ob3(*wsk1);
  delete wsk1;
  wsk1=0;

  const K2 *wsk2 = new K2(ob3);
  ob2=*wsk2;
  cout << ob1 << *wsk2;
  delete wsk2;
  wsk2=0;

  cout << ob2;
  cout << ob2-1.25;
  cout << "*****  3  *****\n" << endl;

  K2 tab[4];
  ifstream ifs("data.txt",std::ifstream::in);
  for (int i=0;i<4;i++){
    string a,b, tmp;
    double c;
    ifs>>a >>b>>c;
    getline(ifs, tmp);
    tab[i]=*(new K2(a,b,c));
    cout << tab[i];
  }
  for (int i=0;i<4;i++){
    tab[i]+=1;
    cout << tab[i];
  }
  cout << "**********4\n";

  tab[1] = tab[1]+ " with sugar";
  tab[3] = "hot " + tab[3];
  for (int i=0;i<4;i++){
    tab[i]+=1;
    cout << tab[i];
  }



  std::cout << "*** koniec" << std::endl;
  return 0;
}
