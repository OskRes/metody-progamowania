#include <iostream>
using namespace std;

class adres {
  string city, code, street;
  int number;
public:
  adres():city("brak miasta"),code("brak kodu"),street("brak ulicy"),number(-1){}
  adres(string miasto, string kod, string ulica, int numer):city(miasto),code(kod),street(ulica),number(numer){}
  friend ostream& operator<<(ostream&, const adres&);
};
ostream& operator<<(ostream& out, const adres& a) {
  return out << "Ulica: " << a.street << " , miasto: " << a.city << ", kod: " << a.code << ", nr: " << a.number << '\n';
}

class osoba {
  string name, surname;
  int years;
  adres* adress;
public:
  osoba():name("brak imienia"),surname("brak nazwiska"),years(-1),adress(new adres()){}
  osoba(const string& imie, const string& nazwisko, int wiek, const adres& a):name(imie),surname(nazwisko),years(wiek),adress(new adres(a)){}
  osoba(const osoba& os):name(os.name),surname(os.surname),years(os.years),adress(new adres(*os.adress)){}
  osoba& operator=(const osoba& a){
      if (this!=&a){
        name=a.name;
        surname = a.surname;
        years =a.years;
        *adress =*a.adress;
      }
      return *this;
  }
  ~osoba(){delete adress;}
  friend ostream& operator<<(ostream&, const osoba&);
};
ostream& operator<<(ostream& out, const osoba& a) {
  return out << "Imię: " << a.name << " , nazwisko: " << a.surname << ", wiek: " << a.years << '\n' << *a.adress;
}

int main() {
  adres* wsk = new adres("Czestochowa", "42-200", "Dabrowskiego", 73);
  cout << *wsk << '\n';

  adres a1(*wsk);
  delete wsk;

  const adres* wsk1 = new adres("Warszawa", "00-950", "Mysliwiecka", 357);
  cout << a1 << '\n';
  cout << *wsk1 << '\n';
  adres a2;
  cout << a2 << '\n';
  a2 = a1;
  cout << a2 << '\n';

  osoba o("Jan", "Kos", 25, *wsk1);

  delete wsk1;

  cout << o << '\n';
  osoba o1(o);
  cout << o1 << '\n';
  osoba o2;
  cout << o2 << '\n';
  o2 = o1;
  cout << o2 << '\n';

  return 0;
}
