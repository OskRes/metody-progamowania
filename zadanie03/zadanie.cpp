#include <iostream>
#include <cmath>

using namespace std;

class punkt {
  double x_, y_;
public:
  punkt() : x_(0), y_(0) {}

  punkt(double a, double b) : x_(a), y_(b) {}

  punkt(const punkt *p) {
    this->x_ = p->x_;
    this->y_ = p->y_;
  }

  double &x() {
    return x_;
  }

  const double &x() const {
    return x_;
  }

  double &y() {
    return y_;
  }

  const double &y() const {
    return y_;
  }

  double odleglosc(const punkt &p) const {
    return sqrt(pow((x() - p.x()), 2) + pow((x() - p.x()), 2));
  }
};

ostream &operator<<(ostream &out, const punkt &a) {
  return out << "x = " << a.x() << ", y = " << a.y() << '\n';
}

class wielobok {
  unsigned iloscWierzcholkow;
  punkt *punkty_;
public:
  wielobok() : iloscWierzcholkow(0), punkty_(0) {}

  wielobok(const punkt *b, const punkt *e) : iloscWierzcholkow(e - b),
                                             punkty_(iloscWierzcholkow ? new punkt[iloscWierzcholkow] : 0) {
    for (unsigned i = 0; i < iloscWierzcholkow; i++) {
      punkty_[i] = new punkt(b + i);
    }
  }

  ~wielobok() {
    delete[] punkty_;
  }

  int ilosc() { return iloscWierzcholkow; }

  void wierzkolki() {
    cout << "początek listy wierzołków" << '\n';
    for (unsigned i = 0; i < iloscWierzcholkow; i++)
      cout << "    " << punkty_[i];
    cout << "koniec listy wierzołków" << '\n';
  }

  const double obwod() const {
    if (iloscWierzcholkow > 2) {
      double tmp = 0.0;
      for (unsigned i = 1; i < iloscWierzcholkow; i++) {
        tmp += (punkty_[i]).odleglosc(punkty_[i - 1]);
        cout << tmp << " i: " << i << endl;
      }
      tmp += (punkty_[iloscWierzcholkow - 1]).odleglosc(punkty_[0]);
      return tmp;
    }
    return 0;
  }

  punkt Punkt(const int i) {
    return punkty_[i - 1];
  }

  void Punkty(const punkt *p1, const punkt *p2) {
    unsigned nowe = p2 - p1;
    if (nowe) {
      punkt *punkty_new = new punkt[iloscWierzcholkow + nowe];
      for (unsigned i = 0; i < iloscWierzcholkow; i++) {
        punkty_new[i] = punkt(punkty_[i]);
      }
      delete[] punkty_;
      punkty_ = punkty_new;
      for (unsigned i = iloscWierzcholkow; i < nowe + iloscWierzcholkow; i++) {
        punkty_[i] = new punkt(p1 + i);
      }
    }
  }
};

int main() {
  punkt p(2, 3);
  cout << p.x() << ' ' << p.y() << '\n';
  p.x() = 1;
  p.y() = 1;
  cout << p.x() << ' ' << p.y() << '\n';
  cout << p.odleglosc(punkt()) << '\n';

  punkt t[] = {punkt(0, 1), punkt(0, 0), punkt(1, 0), punkt(1, 1)};

  wielobok w1(t, t + 4);

  cout << w1.obwod() << '\n';
  w1.Punkt(1) = punkt(0.5, 0.5);
  cout << w1.obwod() << '\n';

  wielobok w2;
  w2.Punkty(t, t + 3);
  cout << w2.obwod() << '\n';
  for (int i = 0; i < w2.ilosc(); ++i)
    cout << w2.Punkt(i).x() << ' ' << w2.Punkt(i).y() << '\n';
  return 0;
}
