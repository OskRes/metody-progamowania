#Zadania z Metod Programowania

### Zadania

1. Numeracja jest zgodna z tymi na zajęciach

2. Wykonywane były zadania od 1 - 6 i 8, ale tu pojawią się wszystkie

3. Zadania oznaczone dodatkowym indeksem są rozwiązaniami takimi jak powinien ten kod wyglądać po roku nauki C++. Podstawowe wersje są w zwykłych numeracjach. Tam pliki są podzielone wg klas/nagłówków więc należy użyć flagi "-I ." w g++. np. "g++ -Wall -pedantic -I . zadanie.cpp -o zadanie.exe"

4. Jeśli chcecie być nabieżąco to polecam naukę narzędzia GIT

5. Wszystkie zadania znajdują się na stronie [dr Jacka Piątkowskiego](http://icis.pcz.pl/~jacekp/mp17/)

### Wygodny skrypt dla użytkowników Linuksa

```
g++ () {
  if [[ $# == 1 ]]; then
    /usr/bin/g++ -Wall -pedantic "$1" -o "$(basename "$1" .cpp).bin" && echo "Kompilacja: OK" && sleep 1 && ./$(basename "$1" .cpp).bin;
  fi<br>
}
```
### Materiały z FB
#### Katalog "Materiały z FB. Niespawdzone, wątpliwej jakości"

1. Pliki pochodzą z FB. **Nie są sprawdzone** i nie wiadomo czy są dobrze.

2. Wybrane mogę sprawdzać i poprawiać ewentualnie usuwać wadliwe materiały.

3. Materiały napisane przeze mnie będą miały dopisane 'oskar'. Np.

 |Plik źródłowy|Plik sprawdzony i sprawny|
 |:---:|:---:|
 |kolokwium.cpp|kolokwium.oskar.cpp|

Za inne pliki z tego katalogu, które nie zawierają w nazwie 'oskar' **nie odpowiadam**, 

#### Pliki ukończone:
* Kolokwium 06.17/kolokwium.oskar.cpp
