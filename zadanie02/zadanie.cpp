#include <iostream>
#include <fstream>
#include <sstream>

class punkt {
  double x_, y_, z_;
public:
  punkt():x_(0),y_(0),z_(0){}
  punkt(const double& a, const double& b, const double& c):x_(a),y_(b),z_(c){}
  double& x(){
    return x_;
  }
  const double& x() const{
    return x_;
  }
  double& y(){
    return y_;
  }
  const double& y() const{
    return y_;
  }
  double& z(){
    return z_;
  }
  const double& z() const{
    return z_;
  }
};

class prostokat {
  double a_, b_;
  punkt* wierzcholek;
public:
    prostokat():a_(0),b_(0),wierzcholek(new punkt()){}
    prostokat(const double wx, const double wy, const double wz, const double a, const double b):a_(0),b_(0),wierzcholek(new punkt(wx, wy, wz)){}
    prostokat(const punkt p, const double a, const double b):a_(a),b_(b),wierzcholek(new punkt(p)){}
    prostokat(const prostokat& src):a_(src.a_),b_(src.a_),wierzcholek(new punkt(src.x(), src.y(), src.z())){};
    prostokat& operator=(const prostokat&);

    ~prostokat(){
      delete wierzcholek;
    }
    double& x(){
      return (*wierzcholek).x();
    }
    const double& x() const{
      return (*wierzcholek).x();
    }
    double& y(){
      return (*wierzcholek).y();
    }
    const double& y() const{
      return (*wierzcholek).y();
    }
    double& z(){
      return (*wierzcholek).z();
    }
    const double& z() const{
      return (*wierzcholek).z();
    }
    double& a(){
      return a_;
    }
    const double& a() const{
      return a_;
    }
    double& b(){
      return b_;
    }
    const double& b() const{
      return b_;
    }
    // możnaby muttalble z nową zmienną dla optymalizacji
    const double pole() const{
      return a_*b_;
    }
};

class graniastoslup {
  prostokat* podstawa;
  double h_;
public:
  graniastoslup():podstawa(new prostokat()),h_(0){}
  graniastoslup(const double wx, const double wy, const double wz, const double a, const double b, const double h):podstawa(new prostokat(wx, wy, wz, a, b)), h_(h){}
  graniastoslup(const punkt& p, const double a, const double b, const double h):podstawa(new prostokat(p, a, b)), h_(h){}
  graniastoslup(const prostokat& p, const double h):podstawa(new prostokat(p)), h_(h){}
  graniastoslup(const graniastoslup& p);
  graniastoslup& operator=(const graniastoslup& p);
  ~graniastoslup(){delete podstawa;};
  double& x(){
    return (*podstawa).x();
  }
  const double& x() const{
    return (*podstawa).x();
  }
  double& y(){
    return (*podstawa).y();
  }
  const double& y() const{
    return (*podstawa).y();
  }
  double& z(){
    return (*podstawa).z();
  }
  const double& z() const{
    return (*podstawa).z();
  }
  double& a(){
    return (*podstawa).a();
  }
  const double& a() const{
    return (*podstawa).a();
  }
  double& b(){
    return (*podstawa).b();
  }
  const double& b() const{
    return (*podstawa).b();
  }
  double& h(){
    return h_;
  }
  const double& h() const{
    return h_;
  }
  double objetosc(){
    return (*podstawa).pole()*h_;
  }
};

int main() {
  using namespace std;
  cout << "part 1\n";

  punkt p1, p2(1,2,3);
  const punkt p3(1.1,2.2,3.3);

  cout << p3.x() << '\t' << p3.y() << '\t' << p3.z() << endl;
  p1.x()=1;
  p1.y()=10;
  p1.z()=100;
  cout << p1.x() << '\t' << p1.y() << '\t' << p1.z() << endl;

  cout << "part 2\n";
  prostokat pr1, pr2(1,2,3,10.5, 20.5);
  const prostokat pr3(p2,5,5);

  cout << pr3.x() << '\t' << pr3.y() << '\t' << pr3.z() << '\n' << pr3.a() << '\t' << pr3.b()<< '\n'<< pr3.pole() << endl;

  pr1.x()=2; pr1.y()=20; pr1.x()=200; pr1.a()= 10; pr1.b()=10;
  cout << pr1.x() << '\t' << pr1.y() << '\t' << pr1.z() << '\n'  << pr1.a() << '\t' << pr1.b() << '\n'  << pr1.pole() << endl;

  cout << "part 3\n";
  graniastoslup g1, g2(1,2,3,10.5,20.5,30.5), g3(p2,100,200,300);
  graniastoslup g4(pr3,5);

  cout << g4.x() << '\t' << g4.y() << '\t' << g4.z() << '\n'  << g4.a() << '\t' << g4.b() << '\t' << g4.h() << '\n'  << g4.objetosc() << endl;

  g1.a()=10; g1.b()=10; g1.h()=10;
  cout << g1.x() << '\t' << g1.y() << '\t' << g1.z() << '\n'  << g1.a() << '\t' << g1.b() << '\t' << g1.h() << '\n'  << g1.objetosc() << endl;
  return 0;
}
