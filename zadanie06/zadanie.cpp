#include <iostream>
#include <fstream>

using namespace std;

class Blad {
  string info;

public:
  Blad() : info("Błąd nieznany") {};

  Blad(string a) : info(a) {};

  friend ostream &operator<<(ostream &, const Blad &);
};

ostream &operator<<(ostream &out, const Blad &bl) {
  return out << "BŁĄD: " << bl.info << endl;
}

class Node {
  int i_;
  double x_, y_;

public:
  Node() : i_(0), x_(0), y_(0) {};

  Node(int i, double a, double b) : i_(i), x_(a), y_(b) {};

  const int i() const { return i_; };

  friend ostream &operator<<(ostream &, Node);
};

ostream &operator<<(ostream &out, Node n) {
  return out << "I: " << n.i_ << " x: " << n.x_ << " y: " << n.y_ << endl;
}

class Polygon {
  int numNodes;
  Node **nodes;

public:
  Polygon() : numNodes(0), nodes(0) {};

  Polygon(ifstream *plik) {
    string s;
    getline(*plik, s);
    cout << s << endl;
    if (s.compare("[POLYGON]"))
      throw new Blad("Brak [POLYGON]");
    getline(*plik, s);
    cout << s << endl;
    if (s.compare("[NUMBER OF NODES]"))
      throw new Blad("Brak [NUMBER OF NODES]");
    *plik >> numNodes;
    cout << numNodes << endl;
    if (numNodes < 0)
      throw new Blad("Nieprawidłowa wartość liczby nodów");
    getline(*plik, s); //do przejścia do nowej linii, bo jest pobierany tylko int
    getline(*plik, s);
    cout << s << endl;
    if (s.compare("[NODES]"))
      throw new Blad("Brak [NODES]");
    nodes = new Node *[numNodes];
    for (int i = 0; i < numNodes; i++) {
      addNode(plik);
    }
    if (s.compare("[POLYGON]"))
      throw new Blad("Brak [POLYGON] - kończącego");
    // int nody;
    // while ()
    // pętla do zliczania wg wzoru i wyświetlenie wyniku
  }

  void addNode(ifstream *s) {
    int i = 0;
    *s >> i;
    i--; //nody liczę w tablicy od 0, a wyświetlam od 1
    if (i < 0 || i >= numNodes)
      throw new Blad("Zła wartość indeksu");
    double xx = 0.0, yy = 0.0;
    *s >> xx;
    *s >> yy;
    nodes[i] = new Node(i + 1, xx, yy);
    cout << *nodes[i];
  }

};

int main(int argc, char *argv[]) try {
  if (argc != 2)
    throw new Blad("Zła liczba argumentów");
  if (string(argv[1]) != "data.txt")
    throw new Blad("Zła nazwa pliku");
  ifstream plik(argv[1]);
  if (plik.bad())
    throw new Blad("Błąd otwarcia pliku");
  Polygon *p = new Polygon(&plik);
  // cout << p;
  plik.close();
  cout << " Jest ok\n";
  return 0;
}
catch (Blad *e) {
  cout << *e;
}
catch (...) {
  cout << "(...): Błąd nieznany - nieużywane" << endl;
}
