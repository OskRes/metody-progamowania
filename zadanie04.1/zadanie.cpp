#include <iostream>
#include "Osoba.h"

int main() {
  adres *wsk = new adres("Czestochowa", "42-200", "Dabrowskiego", 73);
  std::cout << *wsk << '\n';

  adres a1(*wsk);
  delete wsk;

  const adres *wsk1 = new adres("Warszawa", "00-950", "Mysliwiecka", 357);
  std::cout << a1 << '\n';
  std::cout << *wsk1 << '\n';
  adres a2;
  std::cout << a2 << '\n';
  a2 = a1;
  std::cout << a2 << '\n';

  osoba o("Jan", "Kos", 25, *wsk1);

  delete wsk1;

  std::cout << o << '\n';
  osoba o1(o);
  std::cout << o1 << '\n';
  osoba o2;
  std::cout << o2 << '\n';
  o2 = o1;
  std::cout << o2 << '\n';

  return 0;
}
