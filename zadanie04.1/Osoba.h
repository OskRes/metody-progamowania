#include <iostream>
#include "Adres.h"

class osoba {
public:
  std::string name, surname;
  int years;
  adres *adress;

public:
  osoba() : name("brak imienia"), surname("brak nazwiska"), years(-1), adress(new adres()) {}

  osoba(const std::string &imie, const std::string &nazwisko, int wiek, const adres &a)
      : name(imie), surname(nazwisko), years(wiek), adress(new adres(a)) {}

  osoba(const osoba &os) : name(os.name), surname(os.surname), years(os.years), adress(new adres(*os.adress)) {}

  osoba &operator=(const osoba &a) {
    if (this != &a) {
      name = a.name;
      surname = a.surname;
      years = a.years;
      *adress = *a.adress;
    }
    return *this;
  }

  ~osoba() { delete adress; }

  friend std::ostream &operator<<(std::ostream &, const osoba &);
};

std::ostream &operator<<(std::ostream &out, const osoba &a) {
  return out << "Imię: " << a.name << " , nazwisko: " << a.surname << ", wiek: " << a.years << '\n'
             << *a.adress;
}
