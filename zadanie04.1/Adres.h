#include <iostream>

class adres
{
public:
  std::string city, code, street;
  int number;

public:
  adres() : city("brak miasta"), code("brak kodu"), street("brak ulicy"), number(-1) {}
  adres(std::string miasto, std::string kod, std::string ulica, int numer) : city(miasto), code(kod), street(ulica), number(numer) {}
  friend std::ostream &operator<<(std::ostream &, const adres &);
};
std::ostream &operator<<(std::ostream &out, const adres &a)
{
  return out << "Ulica: " << a.street << " , miasto: " << a.city << ", kod: " << a.code << ", nr: " << a.number << '\n';
}
