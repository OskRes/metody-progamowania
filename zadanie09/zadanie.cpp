#include <iostream>
#include <cmath>

class pomiar {
protected:
  std::string nazwa_;
  unsigned long size_;
  double dane_[];
public:
  pomiar();

  pomiar(std::string, double *, double *);

  ~pomiar() {
    delete dane_;
  };

  void dane() const {
    for (int i = 0; i < size_; i++)
      std::cout << dane_[i] << ", ";
    std::cout << std::endl;
  }

};

pomiar::pomiar() : nazwa_("brak"), size_(0), dane_(NULL) {};

pomiar::pomiar(std::string s, double *d1, double *d2) : nazwa_(s), size_(d2 - d1), dane_(new double[d2 - d1]) {
  for (int i = 0; i < d2 - d1; i++)
    dane_[i] = d1[i];
};

void pomiar::dane() const {
  for (int i = 0; i < size_; i++)
    std::cout << dane_[i] << ", ";
  std::cout << std::endl;
}

class analiza : pomiar {
  double sr;
  bool sr_b;
  double wr;
  bool wr_b;
  double od;
  bool od_b;
public:
  analiza();

  analiza(std::string, double *, double *);

  double srednia() const;

  double srednia();

  double variancja() const;

  double variancja();

  double odchylenie() const;

  double odchylenie();

  unsigned long size() const;
};

analiza::analiza() : pomiar(), sr_b(false), wr_b(false), od_b(false) {};

analiza::analiza(std::string s, double *d1, double *d2) : pomiar(s, d1, d2), sr_b(false), wr_b(false), od_b(false) {};

double analiza::srednia() const {
  if (sr_b)
    return this->sr;
  double sr = 0.0;
  for (int i = 0; i < size_; i++)
    sr += dane_[i];
  return (sr / size_);
}

double analiza::srednia() {
  if (sr_b)
    return this->sr;
  double sr = 0.0;
  for (int i = 0; i < size_; i++)
    sr += dane_[i];
  this->sr = (sr / size_);
  this->sr_b = true;
  return this->sr;
}

double analiza::variancja() const {
  if (wr_b)
    return this->wr;
  double wr = 0.0;
  double sr = srednia();
  for (int i = 0; i < size_; i++)
    wr += (dane_[i] - sr) * (dane_[i] - sr);
  return (wr / size_);
}

double analiza::variancja() {
  if (wr_b)
    return this->wr;
  double wr = 0.0;
  double sr = srednia();
  for (int i = 0; i < size_; i++)
    wr += (dane_[i] - sr) * (dane_[i] - sr);
  this->wr = (wr / size_);
  this->wr_b = true;
  return this->wr;
}

double analiza::odchylenie() const {
  if (od_b)
    return this->od;
  return std::sqrt(variancja());
}

double analiza::odchylenie() {
  if (od_b)
    return this->od;
  this->od = std::sqrt(variancja());
  this->od_b = true;
  return this->od;
}

unsigned long analiza::size() const {
  return size_;
}

using namespace std;

int main() {
  double dane1[] = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9};
  double dane2[] = {6.2, 5.0, 3.8, 5.4, 4.6, 6.0, 4.0, 7.5, 2.5};
//...
  analiza a1("Wysokość trawy [ cm ]", dane1, dane1 + 9);
  analiza a2("Moc czynna generatora [ MW ]", dane2, dane2 + 9);
//...
  cout << a1 << a1.srednia() << a1.variancja() << a1.odchylenie();
  cout << a2 << a2.srednia() << a2.variancja() << a2.odchylenie();
  analiza a3("Temperatura w lutym [ C ] ", 27);
  a3[0] = -3;
  a3[5] = -10;
  a3[10] = -8;
  a3[15] = -2;
  a3[20] = 1;
  a3[25] = 2;
  a3[26] = 3;
  cout << a3 << a3.srednia() << a3.variancja() << a3.odchylenie();
  analiza a4;
  cout << a4 << a4.srednia() << a4.variancja() << a4.odchylenie();
  a4 = a3;
  for (unsigned i = 0; i < a4.size(); ++i)
    cout << a4[i] << ‘ ‘;
  cout << a4 << a4.srednia() << a4.variancja() << a4.odchylenie();
  analiza a5(" + korekta temperatury ", 27);
  for (unsigned i = 0; i < a5.size(); ++i)
    a5[i] += 1;
  a4 = a4 + a5;
  cout << a4 << a4.srednia() << a4.variancja() << a4.odchylenie();
}
