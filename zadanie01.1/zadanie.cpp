#include "./Wyjatek.h"
#include "./Student.h"
#include <iostream>
#include <fstream>
#include <sstream>

void studentPrint(const Student *array, const int length)
{
  for (int i = 0; i < length; i++)
    std::cout << array[i];
}
void setImie(Student &st, std::string *value)
{
  st.imie = *value;
}
void setNazwisko(Student &st, std::string *value)
{
  st.nazwisko = *value;
}
void setIndeks(Student &st, int *value)
{
  st.numer = *value;
}

int saveToFile(const Student *studentInput, const std::string *file, int length)
{
  std::ofstream outputFile(*file);
  outputFile << length << '\n';
  for (int i = 0; i < length; i++)
    outputFile << studentInput[i];
  outputFile.close();
  return 0;
}

int readFromFile(Student **studentInput, const std::string &file) try
{
  std::ifstream inputFile(file);
  if (!inputFile.is_open())
    throw Wyjatek("Błąd otwarcia pliku");
  int rows = 0;
  if (!(inputFile >> rows))
    throw Wyjatek("Pierwszy wiersz nie jest liczbą");
  if (rows < 1)
    throw Wyjatek("Zbyt mała liczba wierszy");
  *studentInput = new Student[rows];
  std::string line;
  getline(inputFile, line);
  for (int i = 0; i < rows; i++)
  {
    getline(inputFile, line);
    (*studentInput)[i] = *(new Student(line));
  }
  inputFile.close();
  return rows;
}
catch (Wyjatek &W)
{
  std::cout << W;
  return -1;
}

int main()
{
  using namespace std;
  string sourcePath = "./Student.txt";
  string destinationPath = "./Wynik";
  Student *studentsFromFile = new Student;
  int rows = readFromFile(&studentsFromFile, sourcePath);
  cout << rows << endl;
  studentPrint(studentsFromFile, rows);
  saveToFile(studentsFromFile, &destinationPath, rows);
  cout << studentsFromFile[1];
}
