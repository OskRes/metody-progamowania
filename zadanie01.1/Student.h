#include <iostream>
#include <fstream>
#include <sstream>

struct Student
{
  std::string imie;
  std::string nazwisko;
  unsigned int numer;
  Student() {}
  Student(const std::string &i, const std::string &n, const int nr) : imie(i), nazwisko(n), numer(nr) {}
  Student(const std::string &line)
  {
    std::stringstream oneLineStream(line);
    oneLineStream >> imie;
    oneLineStream >> nazwisko;
    oneLineStream >> numer;
  }
};
std::ostream &operator<<(std::ostream &out, const Student &st)
{
  return out << "* Imię: " << st.imie << ", Nazwisko: " << st.nazwisko << ", Nr indeksu: " << st.numer << '\n';
}