#include "Student.h"
#include "Wyjatek.h"

int main() {
  std::cout << "Student\n";
  std::string *aa = new std::string("aa bb 33");
  std::string bb = "gg hh 66";
  Student *a = new Student(*aa);
  Student *b = new Student("cc dd 44");
  Student *c = new Student("ee", "ff", 55);
  Student *d = new Student(bb);
  Student e(*aa);
  Student f("cc dd 44");
  Student g("ee", "ff", 55);
  Student h(bb);
  Student *i = new Student();
  Student j;
  std::cout << *a;
  std::cout << *b;
  std::cout << *c;
  std::cout << *d;
  std::cout << e;
  std::cout << f;
  std::cout << g;
  std::cout << h;
  std::cout << *i;
  std::cout << j;
  std::cout << "linia 2\n";
  Wyjatek *w1 = new Wyjatek();
  Wyjatek *w2 = new Wyjatek("2");
  Wyjatek w3;
  Wyjatek w4("4");
  std::cout << *w1;
  std::cout << *w2;
  std::cout << w3;
  std::cout << w4;
}
