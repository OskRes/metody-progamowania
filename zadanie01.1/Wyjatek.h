#include <iostream>
class Wyjatek
{
  std::string info;

public:
  Wyjatek() : info("Błąd nieznany"){};
  Wyjatek(std::string a) : info(a){};
  friend std::ostream &operator<<(std::ostream &, const Wyjatek &);
};
std::ostream &operator<<(std::ostream &out, const Wyjatek &W)
{
  return out << "BLĄD: " << W.info << std::endl;
}
