//
// Created by oskar on 03.02.18.
//
// Klasy K1 i K2 są identyczne z wyjątkiem zwracanego typu przy odejmowaniu.
// W klasie K1 odejmowanie K1 - 5 oraz 6 - K1 zwracają int
// W klasie K2 odejmowanie K2 - 5 oraz 6 - K2 zwracają K2 oto cała różnica
// opisy są tylko w wersji K2, w K1 byłyby identyczne
#include <iostream>

using namespace std;

// K1 - przy odejmowaniu zwraca int
class K1 {
  int a, b, c;
public:
  K1() : a(-1), b(-2), c(-3) {}

  K1(int a, int b, int c) : a(a), b(b), c(c) {}

  friend ostream &operator<<(ostream &, const K1 &); //ostream << K1
  friend int operator-(const K1 &, const int &); //int = K1 - int
  friend int operator-(const int &, const K1 &); //int = int - K1
  friend int operator-(const K1 &, const K1 &); //int = K1 - K1
};

ostream &operator<<(ostream &o, const K1 &k1) {
  return o << k1.a << "\t" << k1.b << "\t" << k1.c << endl;
}

int operator-(const K1 &k1, const int &i) {
  return k1.c - i;
}

int operator-(const int &i, const K1 &k1) {
  return i - k1.c;
}

// Pytanie czy ten wynik ma jakiś sens, ale jest dla przykładu
int operator-(const K1 &k1, const K1 &k2) {
  return (k1.a + k1.b + k1.c) - (k2.a + k2.b + k2.c);
}

// K2 - przy odejmowaniu zwraca K2
class K2 {
  int a, b, c;
public:
  // Konstruktor domyślny, tu nigdy nie używany ale dobrze napisana klasa musi go mieć
  K2() : a(-1), b(-2), c(-3) {}

  // Konstruktor inicjujący 3 wartości
  K2(int a, int b, int c) : a(a), b(b), c(c) {}

  // Tu zwracamy referencję do obiektu klasy ostream, bo obiekty klasy ostream (zwykle jest to cout) jest już utworzony
  //  i na nim pracujemy zamiast na kopii, bo szkoda kopiować coś co już jest
  friend ostream &operator<<(ostream &, const K2 &); //ostream << K2 (klasy), w przykładzie cout << k2_ob1
  // Należy zauważyć że nie ma przy operator- referencji. Odejmowanie ma tworzyć NOWY obiekt, który jest wynikiem odejmowania
  //  a nie zmieniać już istniejący
  friend K2 operator-(const K2 &, const int &); //K2 = K2 - int
  friend K2 operator-(const int &, const K2 &); //K2 = int - K2
  friend K2 operator-(const K2 &, const K2 &); //K2 = K2 - K2
};

ostream &operator<<(ostream &o, const K2 &k2) {
  return o << k2.a << "\t" << k2.b << "\t" << k2.c << endl;
}

K2 operator-(const K2 &k2, const int &i) {
  return K2(k2.a - i, k2.b, k2.c);
}

K2 operator-(const int &i, const K2 &k2) {
  return K2(k2.a, k2.b - i, k2.c);
}

K2 operator-(const K2 &k1, const K2 &k2) {
  return K2(k1.a - k2.a, k1.b - k2.b, k1.c - k2.c);
}

int main() {
  cout << "Wersja przeciążenia zwracająca typ int\n";
  K1 k1_ob1(10, 11, 12);
  K1 k1_ob2(1, 2, 3);
  cout << "k1_ob1 =\t" << k1_ob1;
  cout << "k1_ob1 =\t" << k1_ob2;
  cout << "k1_ob1-k1_ob2 =\t" << k1_ob1 - k1_ob2 << endl;
  cout << "k1_ob2 - 6 =\t" << k1_ob2 - 6 << endl;
  cout << "7 - k1_ob2 =\t" << 7 - k1_ob2 << endl;

  cout << "\nWersja przeciążenia zwracająca typ K2\n";
  K2 k2_ob1(10, 11, 12);
  K2 k2_ob2(1, 2, 3);
  cout << "k2_ob2 =\t" << k2_ob1;
  cout << "k2_ob2 =\t" << k2_ob2;
  cout << "k2_ob1-k2_ob2 =\t" << k2_ob1 - k2_ob2;
  cout << "k2_ob1 - 6 =\t" << k2_ob1 - 6;
  cout << "7 - k2_ob1 =\t" << 7 - k2_ob1;

  return 0;
}