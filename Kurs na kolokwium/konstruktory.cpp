//
// Created by oskar on 03.02.18.
//
#include <iostream>

using namespace std;

// K3 - klasa z różnymi konstruktorami
// każdy konstruktor jest podany w 2 wersjach: z listą inicjalizacyjną oraz normalny
class K3 {
  int a;
  string *b;
  string c;
public:
  // Konstruktor domyślny, przypisuje wartości domyślne
  // Warto dać im jakiś sens, który pomoże by testowaniu
  // np. jeśli mamy int i wiemy że będzie od 0 do 100 to dajmy mu np. -1
  // wtedy wiemy, że jeśli wyświetli się -1 to mamy domyślny
  // dla string dobrze dać napis mówiący o domyślnej wartości.
  // Wtedy jeśli mamy cout << obiekt to domyślny coś wyświetli
  // wskaźniki ZAWSZE zerujemy
  K3() : a(-1), b(new string("domyślny b")), c("domyślny c") {}
  // normalny
  //  K3(){
  //    a=-1;
  //    b=0;
  //    c= "domyślny";
  //  }

  // Konstruktor inicjujący 3 wartości gdzie drugie jest wartością
  //  należy utworzyć nowy obiekt i w zapisać jego adres w b
  // gdybyśmy pobrali adres z b1 i użyli go to w momencie gdy ktoś zmieni b1
  // nam zmieni się wartość w utworzonym obiekcie co byłoby niebezpieczne
  K3(int a1, string b1, string c1) : a(a1), b(new string(b1)), c(c1) {}
  // normany
  //  K3(int a1, int b1, string c1){
  //    a=a1;
  //    b = new int(b1);
  //    c=c1;
  //  }

  // Konstruktor inicjujący 3 wartości gdzie drugie jest wskaźnikiem
  K3(int a1, string *b1, string c1) : a(a1), b(new string(*b1)), c(c1) {}
  // normany
  //  K3(int a1, int b1, string c1){
  //    a = a1;
  //    b = new strin(*b1);
  //    c = c1;
  //  }

  // Konstruktor kopiujący
  // dla każdej wartości ze źródła tworzymy kopię i ją ustawiamy
  // jeśli to wskaźnik to trzeba stworzyć nowy obiekt, który jest kopią obiektu wskazywanego przez wskaźnik ze źródła
  // i ustawić jego adres na wskaźniku w obiekcie, który tworzymy
  // jeśli tam nie zrobimy, to wartość wskaźnika ze źródła będzie taka sama jak w utworzonym. Będą to 2 różne obiekty,
  // ale będą pokazywać na ten sam obiekt w pamięci, więc zmiana w jednym zmieni to samo w drugim
//  K3(const K3 &k3) : a(k3.a), b(new int(*k3.b)), c(k3.c) {}
  // normalny
  K3(K3 &k3) {
    a = k3.a;
    b = new string(*k3.b);
    c = k3.c;
  }

  // Operator przypisania operator=
  // nie jest to konstruktor, ale jest trochę podobny

  K3 &operator=(const K3 &in) {
    a = in.a;
    // Usuwamy obiekt, na który wskazuje wskaźnik. Jeśli teraz tego nie zrobimy to gdy zmienimy wartość wskaźnika
    // nie będziemy mieć dostępu do tej zmiennej i będzie sobie gdzieś leżała w pamięci.

    delete b;
    // zwykle po delete należy wyzerować wskaźnik, by nie pokazywał na miejsce do którego nie mamy już dostępu,
    // tu nie trzeba, bo zaraz potem zmieniamy wartość out.b
    //  out.b = 0;
    b = new string(*in.b);
    c = in.c;
    return *this;
  }
};


int main() {
  K3 domyslny;
  K3 inicjujacy1(1, "dwa", "trzy");
  string a = "pięć";
  K3 inicjujacy2(4, &a, "sześć");
  K3 kopiujacy(domyslny);
  K3 przypisany = domyslny;

  return 0;
}