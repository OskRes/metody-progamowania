//
// Created by oskar on 03.02.18.
//
#include <iostream>

using namespace std;

class K4 {
public:
  K4() {
    cout << "Wskaźnik this pokazuje: " << this << " (z konstruktora)\n";
  }

  void pokaz() {
    cout << "Wskaźnik this pokazuje: " << this << " z pokaz()\n";
  };
};

int main() {
  K4 ob1, ob2;
  cout << "wsk1\n";
  K4 *wsk1 = new K4();
  // 1. mamy wskaźnik wsk1,
  // 2. * wyłuskuje wartość spod wskaźnika czyli cały nawias da nam obiekt,
  // 3. po kropce wchodzimy do środka obiektu i uruchamiamy pokaz()
  (*wsk1).pokaz();
  // jeśli chcemy wejść do środka klasy mając wskaźnik zamieniamy kropkę na strzałkę i już
  wsk1->pokaz();
  // oba ... pokaz() robią to samo
  return 0;
}